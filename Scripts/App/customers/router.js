//scripts/app/customers
define(function(require, exports, module) {
    'use strict';

    var Backbone = require('backbone');
    var commonFunction = require('commonfunction');
    require('backbone.subroute');

    var fnSetContentView = function(pathViewFile) {
        var hashtag = '#customers';
        require([pathViewFile + '/view'], function(View) {
            if (View)
                commonFunction.setContentViewWithNewModuleView(new View(), hashtag);
        });
    };

    module.exports = Backbone.SubRoute.extend({
        initialize: function() {
            this.app = {};
        },
        routes: {
            'detail/:id': 'redirectToDetailModule',
            'detail/:id/*subrouter': 'redirectToDetailModule'
        },
        redirectToDetailModule: function(id, subrouter) {
            var self = this;

            require(['./detail/router'], function(Router) {
                if (!self.app.detailRouter) {
                    self.app.detailRouter = new Router(self.prefix + '/detail/:id', {
                        createTrailingSlashRoutes: true
                    });
                }
            });
        }
    });
});
