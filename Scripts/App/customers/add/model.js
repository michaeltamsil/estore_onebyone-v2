// scripts/app/customers/add
define(function(require, exports, module) {
    'use strict';
    var Model = require('backbone.model');

    module.exports = Model.extend({
        urlRoot: 'customer',
        defaults: function(){
            return {
                CustomerName: '',
                CustomerTradingAs: '',
                CustomerABN: '',
                CustomerACN: '',
                DateStart: '',
                AccountOwnerID: '',
                PaymentTermID: '',
                Phone: '',
                Fax: '',
                Mobile: '',
                InvoiceEmail: '',
                PodEmail: '',
                FreightPayableID: '',
                CreditLimit: '',
                TagName: []
            }
        }
    });
});
