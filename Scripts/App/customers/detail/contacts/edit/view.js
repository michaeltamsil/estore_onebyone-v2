// scripts/app/customers/detail/contacts/edit
define(function(require, exports, module) {
    'use strict';
    var LayoutManager = require('layoutmanager');
    var template = require('text!./template.html');
    var commonFunction = require('commonfunction');
    var Model = require('./../model');
    var ladda = require('ladda.jquery');
    var eventAggregator = require('eventaggregator');
    // require('select2');
    require('bootstrap-validator');

    module.exports = LayoutManager.extend({
        className: 'container-fluid',
        template: _.template(template),
        initialize: function() {
            var self = this;
            this.ladda = {};
            this.model = new Model();

            this.model.urlRoot = 'customer/' + commonFunction.getUrlHashSplit(3) + '/contacts';
            this.model.set(this.model.idAttribute, commonFunction.getUrlHashSplit(6));

            this.listenToOnce(this.model, 'sync', function(model) {
                this.render();
                var data = model.toJSON();

                commonFunction.setSelect2ContactType(this).then(function() {
                    self.$('[name="ContactTypeID"]').val(data.ContactType.ContactTypeID).trigger("change");
                });

                this.listenTo(this.model, 'sync', function() {
                    Backbone.history.navigate(commonFunction.getCurrentHashOmitSuffix(1), true);
                });

                this.listenTo(eventAggregator, 'customers/detail/contacts/edit/addAnotherSocialMedia', function() {
                    this.addAnotherSocialMedia();
                }, this);

                this.listenTo(eventAggregator, 'customers/detail/contacts/edit/setButtonAddSocialMedia', function() {
                    var sosMedViews = this.getViews('[obo-view="addAnotherSocialMedia"]').value();

                    _.each(sosMedViews, function(sosMedView) {
                        sosMedView.changeButtonToRemove();
                    });
                    if (sosMedViews.length != 5) {
                        _.last(sosMedViews).changeButtonToAdd();
                        _.first(sosMedViews).showTitle();
                    }

                }, this);

                if (data.SocialMedias && data.SocialMedias.length) {
                    _.each(data.SocialMedias, function(dataSocialMedia) {
                        var model = new Backbone.Model();
                        model.set(dataSocialMedia);

                        self.addAnotherSocialMedia({
                            model: model
                        });
                    });

                } else {
                    this.addAnotherSocialMedia();
                }

                commonFunction.setSelect2Status(this,{
                    selector: '[name="IsActive"]'
                }).then(function(object) {
                    var found = data.IsActive;
                    if (found != undefined){
                        self.$(object.options.selector).val(found.toString()).trigger("change");
                    }
                });

            }, this);

            this.once('afterRender', function() {
                this.model.fetch();
            });

            this.listenTo(this.model, 'request', function() {
                self.$('[obo-dosave]').removeAttr('disabled', 'disabled').html('SAVE');
                self.ladda['obo-dosave'].ladda('stop');
            });

            this.listenTo(this.model, 'sync error', function() {
                self.$('[obo-dosave]').removeAttr('disabled', 'disabled').html('SAVE');
                self.ladda['obo-dosave'].ladda('stop');
            });
        },
        afterRender: function() {
            if (!this.ladda['obo-dosave']) {
                this.ladda['obo-dosave'] = this.$('[obo-dosave]').ladda();
            }

            this.renderValidation();
        },
        renderValidation: function() {
            var self = this;

            this.$('[obo-form]').bootstrapValidator({
                    fields: {
                        PhoneNumber: {
                            validators: {
                                regexp: {
                                    regexp: /^(?=.*[1-9].*)[0-9]{5,10}$/,
                                    message: 'The format of Phone Number is invalid'
                                }
                            }
                        },
                        Mobile: {
                            validators: {
                                regexp: {
                                    regexp: /\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/,
                                    message: 'The format of Mobile Number is invalid'
                                }
                            }
                        },
                        FaxNumber: {
                            validators: {
                                regexp: {
                                    regexp: /^(?=.*[1-9].*)[0-9]{5,10}$/,
                                    message: 'The format of Fax Number is invalid'
                                }
                            }
                        }
                    }
                })
                .on('success.form.bv', function(e) {
                    e.preventDefault();
                    self.doSave();
                });
        },
        doSave: function(e) {
            var data = {};
            data = commonFunction.formDataToJson(this.$('form').serializeArray());
            data.SocialMedia = [];

            eventAggregator.trigger('customers/detail/contacts/edit/view', function(obj) {
                if (obj) {
                    if (obj.SocialMediaID && obj.SocialMediaUrl) {
                        data.SocialMedia.push(obj)
                    } else if (obj.SocialMediaUrl) {
                    }
                } else {
                }
            });

            this.model.save(data);
            this.ladda['obo-dosave'] = this.$('[obo-dosave]').ladda();
            this.ladda['obo-dosave'].ladda('start');
        },
        addAnotherSocialMedia: function(options) {
            var self = this;
            options = options || {};
            require(['./addAnotherSocialMedia/view'], function(View) {

                var length = self.getViews('[obo-view="addAnotherSocialMedia"]').value().length;
                if (!length) {
                    options.showLabel = true;
                }

                var view = new View(options);
                self.insertView('[obo-view="addAnotherSocialMedia"]', view);
                view.render();
            });
        },
        laddaDestroy: function() {
            if (this.ladda['obo-dosave']) {
                this.ladda['obo-dosave'].remove();
                delete this.ladda['obo-dosave'];
            }
        }
    });
});
