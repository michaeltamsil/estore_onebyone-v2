define(function(require, exports, module) {
    'use strict';
    var Model = require('backbone.model');

    module.exports = Model.extend({
        idAttribute: 'CustomerID',
        defaults: function() {
            return {
                Address1: '',
                Address2:'',
                City: '',
                Country: '',
                CountryID: '',
                Email: '',
                FaxNumber: '',
                FirstName: '',
                LastName :'',
                PhoneNumber : '',
                Postcode: '',
                State : '',
                AddressType : {

                },
                IsActive: ''
            }
        }
    });
});
