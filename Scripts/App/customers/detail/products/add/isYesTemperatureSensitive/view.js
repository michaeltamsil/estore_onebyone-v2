// scripts/app/customers/add
define(function(require, exports, module) {
    'use strict';
    var LayoutManager = require('layoutmanager');
    var template = require('text!./template.html');
    var commonFunction = require('commonfunction');
    var commonConfig = require('commonconfig');

    module.exports = LayoutManager.extend({
        template: _.template(template)
    });
});
