//scripts/app/products/detail
define(function(require, exports, module) {
    'use strict';

    var Backbone = require('backbone');
    var Model = require('./../model');
    var commonFunction = require('commonfunction');
    var eventAggregator = require('eventaggregator');
    require('backbone.subroute');

    var fnSetContentView = function(pathViewFile, replaceMainContent) {
        var hashtag = '#products';
        pathViewFile = pathViewFile || commonFunction.getLastSplitHash();
        require(['./' + pathViewFile + '/view'], function(View) {
            var view = new View();

            if (replaceMainContent) {
                commonFunction.setContentViewWithNewModuleView(view, hashtag);
            } else {

                var previousContentView = commonFunction.getContentView();
                var currentContentView = commonFunction.getContentView().getView('');
                var name = previousContentView.getView('') && previousContentView.getView('').name;

                if (name != 'products') {
                    var MainView = require('./view');
                    var mainView = new MainView();
                    commonFunction.setContentViewWithNewModuleView(mainView, hashtag);
                    currentContentView = mainView;
                } else {
                    currentContentView.getView('[obo-menu]').doActiveButton();
                }

                currentContentView.setView('[obo-content]', view);
                view.render();
            }
        });
    };

    module.exports = Backbone.SubRoute.extend({
      initialize: function(){
          this.model = new Model();
          this.stopListening(eventAggregator,'getProductModel');
          this.listenTo(eventAggregator, 'getProductModel', function(callback){
              var id = commonFunction.getUrlHashSplit(3);
              if (!this.model.id || (this.model.id != id)){
                  this.model.set(this.model.idAttribute, id);
                  this.model.once('sync', function(model){
                      callback(model);
                  });
                  this.model.fetch();
              }else{
                  callback(this.model);
              }
          });

      },
        routes: {
            '': 'redirectToProductProfile',
            'product_profile': 'showModule',
            'product_profile/edit': 'showEditModule',
            'orders': 'showModule',
            'orders/detail/:id': 'showOrdersDetail',
            'stock_balance': 'showModule',
            'stock_balance/detail/:id': 'showOrdersDetailStock_balance',
            'stock_transfers': 'showModule',
            'stock_transfers/detail/:id': 'showOrdersDetailStock_transfers',
        },
        redirectToProductProfile: function(){
            debugger;
            this.navigate(this.prefix + '/product_profile', {trigger: true});
        },
        showEditModule: function(){
            fnSetContentView('product_profile/edit', 'replace');
        },
        showOrdersDetail: function(){
            fnSetContentView('orders/detail', 'replace');
        },
        showOrdersDetailStock_balance : function(){
            fnSetContentView('stock_balance/detail', 'replace');
        },
        showOrdersDetailStock_transfers : function(){
            fnSetContentView('stock_transfers/detail', 'replace');
        },
        showModule: function() {
            fnSetContentView();
        }
    });
});
