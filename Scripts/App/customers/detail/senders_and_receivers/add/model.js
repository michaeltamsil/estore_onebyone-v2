// scripts/app/customers/add
define(function(require, exports, module) {
    'use strict';
    var Model = require('backbone.model');

    module.exports = Model.extend({
        urlRoot: 'Customer/SenderReceiver',
        defaults: function() {
            return {
                CustomerID: '',
                FirstName: '',
                LastName: '',
                PhoneNumber: '',
                Email: '',
                Address1: '',
                Address2: '',
                Country: '',
                State: '',
                City: '',
                Postcode: '',
                InsertDate: ''
            }
        }
    });
});
