define(function(require, exports, module) {
    'use strict';
    var LayoutManager = require('layoutmanager');
    var template = require('text!./template.html');

    module.exports = LayoutManager.extend({
        name: 'tasks',
        className: 'container-fluid main-content',
        template: _.template(template),
        afterRender: function() {
            var View = require('./menu/view');
            var view = new View();

            this.insertView('[obo-menu]', view);
            view.render();

            var oboContent = this.getView('[obo-content]');
            if (oboContent) {
                this.oboContent = oboContent;
            } else {
                if (this.oboContent) {
                    this.setView('[obo-content]', this.oboContent);
                }
            }
        }
    });
});
