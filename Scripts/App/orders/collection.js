define(function(require, exports, module) {
    'use strict';

    var Backbone = require('backbone');
    var Model = require('backbone.model');
    var Collection = require('backbone.collection');

    var NewModel = Model.extend();

    module.exports = Collection.extend({
        url: 'dummydata/orders.json',
        model: NewModel
    });
});
