// scripts/app/customers/add
define(function(require, exports, module) {
    'use strict';
    var LayoutManager = require('layoutmanager');
    var template = require('text!./template.html');
    var commonFunction = require('commonfunction');
    var commonConfig = require('commonconfig');
    var Model = require('./model');
    require('select2');
    require('datetimepicker');
    require('moment');
    require('tagsinput');

    module.exports = LayoutManager.extend({
        className: 'container-fluid main-content',
        template: _.template(template),
        initialize: function() {
            var self = this;
            this.model = new Model();
            this.listenTo(this.model, 'sync error', function() {
                self.$('[obo-dosave]').removeAttr('disabled', 'disabled').html('Save');
            });

            this.collectionAccountOwner = commonFunction.getCollectionCustomerAccountOwner();

            this.listenTo(this.collectionAccountOwner, 'sync', function(){
                var collection = this.collectionAccountOwner;
                var selector = this.$('select[name="AccountOwnerID"]');

                selector.empty().append('<option selected disabled>Account Owner</option>');
                collection && _.each(collection.models, function(model) {
                    selector.append(new Option(model.get('AccountName'), model.get('AccountNo')));
                });
            });
        },
        events: {
            'click [obo-dosave]': 'doSave'
        },
        afterRender: function() {
            if (!this.collectionAccountOwner.length) {
                this.collectionAccountOwner.fetch();
            } else {
                this.collectionAccountOwner.trigger('sync');
            }
            
            this.$('select').select2();
            this.$('[date]').datetimepicker({
                defaultDate: new Date(),
                format: commonConfig.datePickerFormat,
            });
            $('[name="AllTags"]').tagsinput({
                tagClass: 'label label-default'
            });
        },
        setCollectionAccountOwner: function() {

        },
        doSave: function(e) {
            if (e && e.currentTarget) {
                $(e.currentTarget).attr('disabled', 'disabled').html('Loading...');
            }
            var data = commonFunction.formDataToJson(this.$('form').serializeArray());
            data.DateStart = data.DateStart.replace(/-/gi, '');
            this.model.save(data);
        }
    });
});
