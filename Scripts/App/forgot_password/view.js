// login/app/forgot_password
define(function(require, exports, module) {
    'use strict';
    require('recaptcha');
    var LayoutManager = require('layoutmanager');
    var template = require('text!./../login/template.html');
    var templateTitle = require('text!./title.html');
    var templateContent = require('text!./content.html');
    var templateResetPasswordRequest = _.template(require('text!./templateresetpasswordrequest.html'));
    var templateEmailAddressNotRegistered = _.template(require('text!./templateemailaddressnotregistered.html'));
    var siteKey = "6LcIiBIUAAAAAGa4pQEFBypPYnrDSE8jn71MAxaZ";

    var commonConfig = require('commonconfig');
    var commonFunction = require('commonfunction');

    var widgetId = undefined;

    if (!window.recaptchaCallback){
        window.recaptchaCallback = function(val) {
            if (widgetId != undefined){
                if (window.grecaptcha){
                    if (!val){
                        $('#recaptcha').empty();
                        widgetId = window.grecaptcha.render(
                            'recaptcha', {
                            sitekey: siteKey,
                            callback: window.recaptchaCallback,
                            'expired-callback': window.recaptchaExpiredCallback
                        });
                    }
                }
            }

            if (val && window.grecaptcha && window.grecaptcha.getResponse(widgetId)) {
                $('button[type="submit"]').removeAttr('disabled');
            } else {
                $('button[type="submit"]').attr('disabled', 'disabled');
            }
        };
    }

    if (!window.recaptchaExpiredCallback){
        window.recaptchaExpiredCallback = function(){
            $('button[type="submit"]').attr('disabled', 'disabled');
        }
    }

    module.exports = LayoutManager.extend({
        className: 'container center-middle',
        template: _.template(template),
        initialize: function() {
            this.options = this.options || {};
            this.options.loadingRecaptcha = false;
            this.options.firstTimeRecaptcha = true;
        },
        serialize: function() {
            var templatingContent = _.template(templateContent);

            return {
                title: templateTitle,
                content: templatingContent({
                    siteKey: siteKey
                }),
            }
        },
        events: {
            'submit form': 'doSubmit',
            'click [obo-reload]': 'doReload'
        },
        afterRender: function() {
            var self = this;
            if (!this.options.loadingRecaptcha) {
                this.options.loadingRecaptcha = true;

                var fnTimeoutLoadingRecaptcha = function() {

                    window.setTimeout(function() {
                        if (window.grecaptcha && window.grecaptcha.render) {
                            self.options.loadingRecaptcha = false;
                            if (self.options.firstTimeRecaptcha && self.$('#recaptcha iframe').length) {
                                self.options.firstTimeRecaptcha = 2;
                            } else {
                                self.$('#recaptcha').empty();

                                widgetId = window.grecaptcha.render('recaptcha', {
                                    sitekey: siteKey,
                                    callback: window.recaptchaCallback,
                                    'expired-callback': window.recaptchaExpiredCallback
                                });

                            }
                        } else {
                            fnTimeoutLoadingRecaptcha();
                        }
                    }, 300);
                }

                fnTimeoutLoadingRecaptcha();
            }
        },
        doSubmit: function(e) {
            e.preventDefault();
            var self = this;
            if (grecaptcha && grecaptcha.getResponse(widgetId)){
                var data = commonFunction.formDataToJson($(e.currentTarget).serializeArray());
                data.GRecaptchaResponse = grecaptcha.getResponse(widgetId);
                delete data['g-recaptcha-response'];

                $.ajax({
                    url: commonConfig.requestServer + 'Account/ForgotPassword',
                    method: 'POST',
                    data: data,
                    success: function() {
                        self.$('[obo-content]').html(templateResetPasswordRequest({
                            UserName: data.UserName
                        }));
                    },
                    error: function(xhr) {
                        self.$('[obo-content]').html(templateEmailAddressNotRegistered({
                            UserName: data.UserName,
                            message: xhr.responseText || 'something went wrong'
                        }));
                    }
                });
            }else{
                window.recaptchaCallback();
            }
            return false;
        },
        doReload: function() {
            this.render();
        }
    });
});
