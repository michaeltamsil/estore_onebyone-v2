//scripts/app
define(function(require, exports, module) {
    'use strict';

    require('backbone');
    var commonFunction = require('commonfunction');
    // var originalFn = Backbone.history.loadUrl;

    var fnSetContentView = function(pathViewFile, hashtag, options) {
        require(['./' + pathViewFile + '/view'], function(View) {
            if (View)
                commonFunction.setContentViewWithNewModuleView(new View(), hashtag, options);
        });
    };

    // Backbone.history.loadUrl = function() {
    //     var doneOriginalFn = false;
    //     var previousFragment = Backbone.history.fragment;
    //     var args = arguments;
    //     var hash = window.location.hash;
    //     if (!doneOriginalFn)
    //         return originalFn.apply(this, arguments);
    // };

    module.exports = Backbone.Router.extend({
        initialize: function() {
            this.app = {};
        },
        routes: {
            '': 'showMainMenu',
            'login': 'showLogin',
            'forgot_password': 'showForgotPassword',
            'reset_password': 'showResetPassword',
            'tasks': 'redirectToTasksModule',
            'tasks/*subrouter': 'redirectToTasksModule',
            'customers': 'showCustomers',
            'customers/add': 'showAddCustomers',
            'customers/detail/*subrouter': 'redirectToCustomersModule',
            'products': 'showProducts',
            'products/add': 'showAddProducts',
            'products/*subrouter': 'redirectToProductsModule',

            'storage/warehouse': 'redirectToStorageWarehouseModule',
            'storage/warehouse/*subrouter': 'redirectToStorageWarehouseModule',

            'storage/inward_goods': 'showStorageInwardGoods',
            'storage/inward_goods/add': 'showAddStorageInwardGoods',
            'storage/inward_goods/detail/*subrouter': 'redirectToStorageInwardGoodsModule',

            'orders': 'showOrders',
            'orders/add': 'showAddOrders',
            'orders/edit': 'showEditOrders',
            'orders/detail/*subrouter': 'redirectToOrderModule',

            'administration': 'redirectToAdministrationModule',
            'administration/*subrouter': 'redirectToAdministrationModule',

            'consignments': 'redirectToConsignmentsModule',
            'consignments/*subrouter': 'redirectToConsignmentsModule',

            'configuration': 'redirectToConfigurationModule',
            'configuration/*subrouter': 'redirectToConfigurationModule',

            // 'administration/employees':'redirectToAdministrationEmployeesModule',
            // 'administration/employees/*subrouter': 'redirectToAdministrationEmployeesModule',
            // 'administration/roles':'redirectToAdministrationRolesModule',
            // 'administration/roles/*subrouter': 'redirectToAdministrationRolesModule',
            // 'administration/users':'redirectToAdministrationUsersModule',
            // 'administration/users/*subrouter': 'redirectToAdministrationUsersModule',
            '*actions': 'notFound'
        },
        start: function() {
            Backbone.history.start();
        },
        showLogin: function() {
            fnSetContentView('./login');
        },
        showForgotPassword: function() {
            fnSetContentView('./forgot_password');
        },
        showResetPassword: function() {
            fnSetContentView('./reset_password');
        },
        showMainMenu: function() {
            fnSetContentView('mainmenu', '#');
        },
        redirectToTasksModule: function() {
            //fnSetContentView('./tasks', '#tasks');
            var self = this;

            if (!this.app.tasksRouter) {
                require(['./tasks/router'], function(Router) {
                    self.app.tasksRouter = new Router('tasks', {
                        createTrailingSlashRoutes: true
                    });
                });
            }
        },
        showAddCustomers: function() {
            fnSetContentView('customers/add', '#customers');
        },
        showCustomers: function() {
            fnSetContentView('customers', '#customers');
        },
        redirectToCustomersModule: function() {
            var self = this;

            if (!this.app.customersRouter) {
                require(['./customers/router'], function(Router) {
                    self.app.customersRouter = new Router('customers', {
                        createTrailingSlashRoutes: true
                    });
                });
            }
        },
        showProducts: function() {
            fnSetContentView('products', '#products');
        },
        showAddProducts: function() {
            fnSetContentView('products/add', '#products');
        },
        redirectToProductsModule: function() {
            var self = this;

            if (!this.app.productsRouter) {
                require(['./products/router'], function(Router) {
                    self.app.productsRouter = new Router('products', {
                        createTrailingSlashRoutes: true
                    });
                });
            }
        },

        redirectToStorageWarehouseModule: function() {
            var self = this;
            if (!this.app.storageRouter) {
                require(['./storage/warehouse/router'], function(Router) {
                    self.app.storageWarehouseRouter = new Router('storage/warehouse', {
                        createTrailingSlashRoutes: true
                    });
                });
            }
        },

        showStorageInwardGoods: function() {
            fnSetContentView('storage/inward_goods', '#storage/inward_goods')
        },
        showAddStorageInwardGoods: function() {
            fnSetContentView('storage/inward_goods/add', '#inward_goods')
        },
        redirectToStorageInwardGoodsModule: function() {
            var self = this;
            if (!this.app.storageRouter) {
                require(['./storage/inward_goods/router'], function(Router) {
                    self.app.storageWarehouseRouter = new Router('storage/inward_goods', {
                        createTrailingSlashRoutes: true
                    });
                });
            }
        },

        showOrders: function() {
            fnSetContentView('orders', '#orders')
        },
        showAddOrders: function() {
            fnSetContentView('orders/add', '#orders')
        },
        showEditOrders: function() {
            fnSetContentView('orders/edit', '#orders')
        },
        redirectToOrderModule: function() {
            var self = this;

            if (!this.app.ordersRouter) {
                require(['./orders/router'], function(Router) {
                    self.app.ordersRouter = new Router('orders', {
                        createTrailingSlashRoutes: true
                    });
                });
            }
        },

        // redirectToTasksModule: function() {
        //     fnSetContentView('my_tasks', '#tasks');
        //     var self = this;
        //
        //     if (!this.app.tasksRouter) {
        //         require(['./administration/router'], function(Router) {
        //             self.app.tasksRouter = new Router('tasks', {
        //                 createTrailingSlashRoutes: true
        //             });
        //         });
        //     }
        // },

        redirectToAdministrationModule: function() {
            var self = this;

            if (!this.app.administrationRouter) {
                require(['./administration/router'], function(Router) {
                    self.app.administrationRouter = new Router('administration', {
                        createTrailingSlashRoutes: true
                    });
                });
            }
        },

        redirectToConsignmentsModule: function() {
            var self = this;

            if (!this.app.consignmentsRouter) {
                require(['./consignments/router'], function(Router) {
                    self.app.consignmentsRouter = new Router('consignments', {
                        createTrailingSlashRoutes: true
                    });
                });
            }
        },

        redirectToConfigurationModule: function() {
            var self = this;

            if (!this.app.configurationRouter) {
                require(['./configuration/router'], function(Router) {
                    self.app.configurationRouter = new Router('configuration', {
                        createTrailingSlashRoutes: true
                    });
                });
            }
        },

        notFound: function() {
            require(['./errorpages/notfound/view'], function(View) {
                commonFunction.setContentViewWithNewModuleView(new View(), '#');
            });
        },
    });
});
