//scripts/app/administration/router
define(function(require, exports, module) {
    'use strict';

    var Backbone = require('backbone');
    var Model = require('./model');
    var commonFunction = require('commonfunction');

    require('backbone.subroute');

    var fnSetContentView = function(pathViewFile, replaceMainContent, options) {

        var hashtag = '#administration/' + ((options && options.appendHashTag) || pathViewFile);

        require(['./' + pathViewFile + '/view'], function(View) {
            var view = new View();

            if (replaceMainContent) {
                commonFunction.setContentViewWithNewModuleView(view, hashtag);
            } else {
                var previousContentView = commonFunction.getContentView();
                var currentContentView = commonFunction.getContentView().getView('');
                var name = previousContentView.getView('') && previousContentView.getView('').name;

                if (name != 'administration') {
                    var MainView = require('./view');
                    var mainView = new MainView();
                    mainView.setView('[obo-content]', view);
                    commonFunction.setContentViewWithNewModuleView(mainView, hashtag);
                } else {
                    currentContentView.getView('[obo-menu]').doActiveButton();
                    currentContentView.setView('[obo-content]', view);
                    view.render();
                }
            }
        });
    };

    module.exports = Backbone.SubRoute.extend({
        routes: {
            '': 'redirectToUsers',
            'users': 'showUsers',
            'users/add': 'showUsersAdd',
            'roles': 'showRoles',
            'roles/add': 'showRolesAdd',
            'employees': 'showEmployees',
            '*subrouter': 'redirectToUsers'
        },
        showUsers: function() {
            fnSetContentView('users', 'replace');
        },
        showUsersAdd: function() {
            fnSetContentView('users/add', 'replace', {
                appendHashTag: 'users'
            });
        },
        showRoles: function() {
            fnSetContentView('roles', 'replace');
        },
        showRolesAdd: function() {
            fnSetContentView('roles/add', 'replace',{
                appendHashTag: 'roles'
            });
        },
        showEmployees: function() {
            fnSetContentView('employees', 'replace');
        },
        redirectToUsers: function() {
            this.navigateBackbonePrototype(commonFunction.getCurrentHashToLevel(1).replace('#', '') + '/users', {
                trigger: true,
                replace: true
            });
        }
    });
});
