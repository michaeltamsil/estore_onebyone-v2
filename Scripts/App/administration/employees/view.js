// scripts/app/administration/employees/view
define(function(require, exports, module) {
    'use strict';
    var Menu = require('menu');
    var template = require('text!./template.html');

    module.exports = Menu.extend({
        template: _.template(template)
    });
});
