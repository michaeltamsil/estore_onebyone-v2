//scripts/app/storage/detail
define(function(require, exports, module) {
    'use strict';

    var Backbone = require('backbone');
    var commonFunction = require('commonfunction');
    require('backbone.subroute');

    var fnSetContentView = function(pathViewFile, replaceMainContent) {
        var hashtag = '#storage';

        require(['./' + pathViewFile + '/view'], function(View) {
            var view = new View();

            if (replaceMainContent) {
                commonFunction.setContentViewWithNewModuleView(view, hashtag);
            } else {

                var previousContentView = commonFunction.getContentView();
                var currentContentView = commonFunction.getContentView().getView('');
                var name = previousContentView.getView('') && previousContentView.getView('').name;

                if (name != 'storagedetail') {
                    var MainView = require('./view');
                    var mainView = new MainView();
                    commonFunction.setContentViewWithNewModuleView(mainView, hashtag);
                    currentContentView = mainView;
                } else {
                    currentContentView.getView('[obo-menu]').doActiveButton();
                }

                currentContentView.setView('[obo-content]', view);
                view.render();
            }
        });
    };

    module.exports = Backbone.SubRoute.extend({
        initialize: function(){
            this.app = {
                storageLocation:{
                    detail: {}
                }
            }

        },
        routes: {
            // '': 'redirecDetail',
            'detail': 'showDetail',
            'warehouse_information/edit': 'showWarehouseInformationEdit',
        },
        redirecDetail: function(){
            this.navigateBackbonePrototype(commonFunction.getCurrentHashToLevel(3).replace('#', '') + '/warehouse_information', {trigger: true, replace: true});
        },
        showDetail: function() {
            fnSetContentView('detail');
        },
        showWarehouseInformationEdit: function(){
            fnSetContentView('warehouse_information/edit', 'replace');
        },
        showStorageLocation: function() {
            fnSetContentView('storage_location');
        },
        redirectToStorageLocationDetailModule: function(){
            var self = this;
            require(['./storage_location/detail/router'], function(Router) {
                if (!self.app.storageLocation.detail.router) {
                    self.app.storageLocation.detail.router = new Router(self.prefix + '/storage_location/detail/:id', {
                        createTrailingSlashRoutes: true
                    });
                }
            });
        }
    });
});
