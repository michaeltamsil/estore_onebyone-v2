// scripts/app/customers/detail/customer_profile
define(function(require, exports, module) {
    'use strict';
    var LayoutManager = require('layoutmanager');
    var template = require('text!./template.html');
    // var Model = require('./../model');
    var commonFunction = require('commonfunction');
    require('moment');

    module.exports = LayoutManager.extend({
        className: 'container-fluid',
        template: _.template(template),
        initialize: function() {
            // this.model = new Model();
            // this.model.set(this.model.idAttribute, commonFunction.getUrlHashSplit(3));
            // this.listenToOnce(this.model, 'sync', function(model) {
            //     this.render();
            // }, this);
            // this.once('afterRender', function() {
            //     this.model.fetch();
            // });
        }
    });
});
