//scripts/app/storage
define(function(require, exports, module) {
    'use strict';

    var Backbone = require('backbone');
    var commonFunction = require('commonfunction');
    require('backbone.subroute');

    var fnSetContentView = function(pathViewFile) {
        var hashtag = '#storage/warehouse';
        require([pathViewFile + '/view'], function(View) {
            if (View)
                commonFunction.setContentViewWithNewModuleView(new View(), hashtag);
        });
    };

    module.exports = Backbone.SubRoute.extend({
        initialize: function() {
            this.app = {};
        },
        routes: {
            '': 'showList',
            'add': 'showAdd',
            'detail/:id': 'redirectToDetailModule',
            'detail/:id/*subrouter': 'redirectToDetailModule',
            '*subrouter': 'redirectToShowList'
        },
        showList: function(){
            fnSetContentView('.', '#storage/warehouse')
        },
        showAdd: function(){
            fnSetContentView('./add', '#storage/warehouse')
        },
        redirectToDetailModule: function(id, subrouter) {
            var self = this;

            require(['./detail/router'], function(Router) {
                if (!self.app.detailRouter) {
                    self.app.detailRouter = new Router(self.prefix + '/detail/:id', {
                        createTrailingSlashRoutes: true
                    });
                }
            });
        },
        redirectToShowList: function(){
            this.navigateBackbonePrototype(commonFunction.getCurrentHashToLevel(2).replace('#', ''), {
                trigger: true,
                replace: true
            });
        }
    });
});
