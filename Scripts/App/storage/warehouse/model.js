define(function(require, exports, module) {
    'use strict';

    var Model = require('backbone.model');

    module.exports = Model.extend({
        idAttribute: 'WarehouseID',
        // urlRoot: 'dummydata/Storage/Warehouse'
        urlRoot: 'Storage/Warehouse',
        defaults: function() {
            return {
                WarehouseName: '',
                WarehouseManagerPhoneNumber: '',
                WarehouseManagerLastName: '',
                WarehouseManagerFirstName: '',
                WarehouseManagerFaxNumber: '',
                WarehouseManagerEmailAddress: '',
                WarehouseDescription: '',
                State: '',
                PostCode: '',
                Phone: '',
                Mobile: '',
                ManagementType: {
                    WarehouseManagementTypeName:''
                },
                WarehouseManagementType : {
                    WarehouseManagementTypeName: ''
                },
                WarehouseOwner:{
                    AccountName:''
                },
                WarehouseManager:{
                    WarehousePhoneNumber: '',
                    WarehouseFaxNumber: '',
                    WarehouseEmail: ''
                },
                IsActive: '',
                Fax: '',
                Email: '',
                Country: {
                    CountryName:''
                },
                City: '',
                Address2: '',
                Address1: ''
            }
        }

    });
});
