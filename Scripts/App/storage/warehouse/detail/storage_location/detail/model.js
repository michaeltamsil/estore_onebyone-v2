define(function(require, exports, module) {
    'use strict';

    var Model = require('backbone.model');

    module.exports = Model.extend({
        idAttribute: 'StorageLocationID',
        urlRoot: 'storage/storageLocation',
        defaults: function(){
            return {
                StorageLocationID: '',
                Bay: '',
                Building: '',
                IsActive: false,
                Isle: '',
                LocationCode: '',
                LocationScetchFileName: '',
                LocationScetchOriginalFileName: '',
                LocationScetchURL: '',
                MaxHeight: 0,
                MaxLength: 0,
                MaxQuantity: 0,
                MaxWeightKgs: 0,
                MaxWidth: 0,
                Tier: '',
                ZoneType: {
                    ZoneTypeID: 0,
                    ZoneTypeName: ''
                },
                ZoneArea: {
                    ZoneAreaID: 0,
                    ZoneAreaName: ''
                },
                SKUType:{
                    SKUTypeName: ''
                },
                IsActive: false,
                StorageWarehouse: {
                    WarehouseID:''
                }
            }
        }
    });
});
