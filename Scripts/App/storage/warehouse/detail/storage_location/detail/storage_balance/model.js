define(function(require, exports, module) {
    'use strict';
    var Model = require('backbone.model');

    module.exports = Model.extend({
        idAttribute: 'StockBalanceID',
        // urlRoot :'Storage/StockBalance',
        defaults: function() {
            return {
                Date: '',
                ProductName:'',
                Balance:'',
                QtyIn:'',
                QtyOut:''
            }
        }
    });
});
