// scripts/app/customers/detail/contacts
define(function(require, exports, module) {
    'use strict';
    var View = require('./../../../../commons/view');
    var template = require('text!./template.html');
    var commonFunction = require('commonfunction');
    var Table = require('./table/table');
    var Collection = require('./collection');
    var Paging = require('paging');
    require('bootstrap-validator');
    var Filter = require('./filter/view');

    module.exports = View.extend({
        template: _.template(template),
        initialize: function() {
            this.table = new Table({
                collection: new Collection()
            });
            this.table.collection.url = 'Storage/'+ commonFunction.getUrlHashSplit(4) +'/StockTransfer';

            this.filter = new Filter({
                collection: this.table.collection
            });

            this.paging = new Paging({
                collection: this.table.collection
            });

            this.on('cleanup', function() {
                this.table.destroy();
                this.modalDialog && this.modalDialog.remove && this.modalDialog.remove();
            }, this)

        },
        afterRender: function() {
            this.insertView('[obo-filter]', this.filter);
            this.filter.render();

            this.$('[obo-table]').append(this.table.el);
            this.table.render();

            this.insertView('[obo-paging]', this.paging);
            this.paging.render();

            this.table.collection.fetch();
            // this.$("form").submit(function(e) {
            //     e.preventDefault();
            // });
            // this.renderValidation();
        },
        // renderValidation: function() {
        //     $('[obo-form]').bootstrapValidator({})
        //         .on('success.form.bv', function(e) {
        //             $('#success_message').slideDown({
        //                 opacity: "show"
        //             }, "slow") // Do something ...
        //             $('[obo-form]').data('bootstrapValidator').resetForm();
        //
        //             // Prevent form submission
        //             e.preventDefault();
        //
        //             // Get the form instance
        //             var $form = $(e.target);
        //
        //             // Get the BootstrapValidator instance
        //             var bv = $form.data('bootstrapValidator');
        //
        //             // Use Ajax to submit form data
        //             $.post($form.attr('action'), $form.serialize(), function(result) {
        //                 console.log(result);
        //             }, 'json');
        //         });
        // },
        showEditFilters: function() {
            var self = this;
            require(['./modaldialogeditfilter/view'], function(ModalDialog) {
                self.showModalDialog(ModalDialog);
            });
        }
    });
});
