define(function(require, exports, module) {
    'use strict';
    var LayoutManager = require('layoutmanager');
    var Model = require('backbone.model');
    var template = require('text!./template.html');

    var commonConfig = require('commonconfig');
    var commonFunction = require('commonfunction');
    var Cookies = require('Cookies');
    var cswall = require('sweetalert');

    module.exports = LayoutManager.extend({
        el: false,
        initialize: function() {
            var NewModel = Model.extend({
                urlRoot: 'Account/Logout'
            });
            this.model = new NewModel();

            this.listenTo(this.model, 'sync', function() {
                cswall.close();
                window.location.hash = 'login';

            });
        },
        template: _.template(template),
        events: {
            'click .navbar-collapse a[href]:not([href="#"])': 'bindingEventNagivate',
            'click [obo-logout]': 'doLogout'
        },
        afterRender: function() {
            if (navigator.userAgent.toLowerCase().indexOf('ie') > 1 || navigator.userAgent.toLowerCase().indexOf('firefox') > 1) {
                $.Bootstrap.pushMenu.activate("[data-toggle='offcanvas']");
            }
            var firstName = Cookies.get(commonConfig.cookieFields.firstName);
            if (firstName) {
                this.$('[name="firstName"]').text(firstName);
            }
        },
        bindingEventNagivate: function(e) {
            e.preventDefault();
            var href = $(e.currentTarget).attr('href');

            var ret = Backbone.history.navigate(href, true);

            if (ret === undefined) {
                Backbone.history.loadUrl(href);
            }
        },
        doLogout: function() {
            var self = this;
            swal({
                    title: "Are you sure you want to log out?",
                    text: "Press Cancel if you want to continue work. </br> Or Press Yes to logout current user.",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: "btn-info",
                    confirmButtonText: "Yes",
                    closeOnConfirm: false,
                    html: true
                },
                function() {
                    _.each(commonConfig.cookieFields, function(item) {
                        Cookies.remove(item);
                    });
                    
                    self.model.save();
                });
        }
    });
});
