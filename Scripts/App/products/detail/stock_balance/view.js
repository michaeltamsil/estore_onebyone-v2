// scripts/app/products/stock_balance
define(function(require, exports, module) {
    'use strict';
    var LayoutManager = require('layoutmanager');
    var template = require('text!./template.html');
    require('select2');

    module.exports = LayoutManager.extend({
        template: _.template(template),
        afterRender: function(){
          this.$('select').select2();
        }
    });
});
