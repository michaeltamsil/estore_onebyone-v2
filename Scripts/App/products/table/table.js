// scripts/app/customers/list
define(function(require, exports, module) {
    'use strict';
    var Marionette = require('marionette');
    var template = require('text!./table.html');
    var Tbody = require('./tbody');
    var SortingButton = require('sorting.button');

    module.exports = Marionette.View.extend({
        tagName: 'table',
        className: 'table table-unbordered',
        template: _.template(template),
        regions: {
            body: {
                el: 'tbody',
                replaceElement: true
            },
            ProductName: {
                el: '[data-parampaging-sortfield="ProductName"]'
            },
            ProductCode:{
              el: '[data-parampaging-sortfield="ProductCode"]'
            },
            CustomerName: {
                el: '[data-parampaging-sortfield="CustomerName"]'
            },
            IsDangerousGoods: {
                el: '[data-parampaging-sortfield="IsDangerousGoods"]'
            },
            UnitValue: {
                el: '[data-parampaging-sortfield="UnitValue"]'
            }
        },
        onRender: function() {
            var self = this;
            this.showChildView('body', new Tbody({
                collection: this.collection
            }));

            _.each(this.$('thead > tr > th > [data-parampaging-sortfield]'), function(item) {
                var SortField = $(item).attr('data-parampaging-sortfield');
                self.showChildView(SortField, new SortingButton({
                    SortField: SortField,
                    collection: self.collection
                }));

            }, this);


        }
    });
});
