// scripts/app/customers/
define(function(require, exports, module) {
    'use strict';
    var DefaultModule = require('defaultmodule');
    var template = require('text!./template.html');
    var Table = require('./table/table');
    var Collection = require('./collection');
    var Filter = require('./filter/view');
    var Paging = require('paging');
    var ModalDialog = require('./modaldialogeditfilter/view');
    require('select2');

    module.exports = DefaultModule.extend({
        template: _.template(template),
        initialize: function() {
            var self = this;
            this.table = new Table({
                collection: new Collection()
            });

            this.filter = new Filter({
                collection: this.table.collection
            });

            this.paging = new Paging({
                collection: this.table.collection
            });

            this.ModalDialog = ModalDialog

            this.on('cleanup', function() {
                this.table.remove();
            });
        },
        afterRender: function() {
            this.insertView('[obo-filter]', this.filter);
            this.filter.render();

            this.$('[obo-table]').append(this.table.el);
            this.table.render();

            this.insertView('[obo-paging]', this.paging);
            this.paging.render();

            this.table.collection.fetch();
            this.$('select').select2();
        }
    });
});
