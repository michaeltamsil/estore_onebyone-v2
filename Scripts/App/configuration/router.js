//scripts/app/configuration/router
define(function(require, exports, module) {
    'use strict';

    var Backbone = require('backbone');
    var Model = require('./model');
    var commonFunction = require('commonfunction');

    require('backbone.subroute');

    var fnSetContentView = function(pathViewFile, replaceMainContent, options) {

        var hashtag = '#configuration/' + ((options && options.appendHashTag) || pathViewFile);

        require(['./' + pathViewFile + '/view'], function(View) {
            var view = new View();

            if (replaceMainContent) {
                commonFunction.setContentViewWithNewModuleView(view, hashtag);
            } else {
                var previousContentView = commonFunction.getContentView();
                var currentContentView = commonFunction.getContentView().getView('');
                var name = previousContentView.getView('') && previousContentView.getView('').name;

                if (name != 'configuration') {
                    var MainView = require('./view');
                    var mainView = new MainView();
                    mainView.setView('[obo-content]', view);
                    commonFunction.setContentViewWithNewModuleView(mainView, hashtag);
                } else {
                    currentContentView.getView('[obo-menu]').doActiveButton();
                    currentContentView.setView('[obo-content]', view);
                    view.render();
                }
            }
        });
    };

    module.exports = Backbone.SubRoute.extend({
        routes: {
            '': 'redirectToConfig_Settings',
            'config_settings': 'showConfig_Settings',
            'dangerous_goods': 'showDangerous_Goods',
            'dangerous_goods/add': 'showDangerous_GoodsAdd',
            'depot': 'showDepot',
            'depot/add': 'showDepotAdd',
            'sub_contractors': 'showSub_Contractors',
            'sub_contractors/add': 'showSub_ContractorsAdd',
            'vehicles': 'showVehicles',
            '*subrouter': 'redirectToConfig_Settings'
        },
        showConfig_Settings: function() {
            fnSetContentView('config_settings', 'replace');
        },
        showDangerous_Goods: function() {
            fnSetContentView('dangerous_goods', 'replace');
        },
        showDangerous_GoodsAdd: function() {
            fnSetContentView('dangerous_goods/add', 'replace', {
                appendHashTag: 'dangerous_goods'
            });
        },
        showDepot: function() {
            fnSetContentView('depot', 'replace');
        },
        showDepotAdd: function() {
            fnSetContentView('depot/add', 'replace',{
                appendHashTag: 'depot'
            });
        },
        showSub_Contractors: function() {
            fnSetContentView('sub_contractors', 'replace');
        },
        showSub_ContractorsAdd: function() {
            fnSetContentView('sub_contractors/add', 'replace',{
                appendHashTag: 'sub_contractors'
            });
        },
        showVehicles: function() {
            fnSetContentView('vehicles', 'replace');
        },
        redirectToConfig_Settings: function() {
            this.navigateBackbonePrototype(commonFunction.getCurrentHashToLevel(1).replace('#', '') + '/config_settings', {
                trigger: true,
                replace: true
            });
        }
    });
});
