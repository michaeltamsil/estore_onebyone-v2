// scripts/app/customers/list
define(function(require, exports, module) {
    'use strict';
    var Marionette = require('marionette');
    var template = require('text!./table.html');
    var Tbody = require('./tbody');
    var SortingButton = require('sorting.button');

    module.exports = Marionette.View.extend({
        tagName: 'table',
        className: 'table table-unbordered',
        template: _.template(template),
        regions: {
            body: {
                el: 'tbody',
                replaceElement: true
            },
            UnNumber: {
                el: '[data-parampaging-sortfield="UnNumber"]'
            },
            Class: {
                el: '[data-parampaging-sortfield="Class"]'
            },
            SubRisk: {
                el: '[data-parampaging-sortfield="SubRisk"]'
            },
            Hazchem: {
                el: '[data-parampaging-sortfield="Hazchem"]'
            },
            ChemicalName:{
                el: '[data-parampaging-sortfield="ChemicalName"]'
            }
        },
        onRender: function() {
            var self = this;
            this.showChildView('body', new Tbody({
                collection: this.collection
            }));

            _.each(this.$('thead > tr > th > [data-parampaging-sortfield]'), function(item) {
                var SortField = $(item).attr('data-parampaging-sortfield');
                self.showChildView(SortField, new SortingButton({
                    SortField: SortField,
                    collection: self.collection
                }));
            }, this);
        }
    });
});
