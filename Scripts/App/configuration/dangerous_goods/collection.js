define(function(require, exports, module) {
    'use strict';
    var Backbone = require('backbone');
    var Collection = require('backbone.collection.paging');

    module.exports = Collection.extend({
        url: 'dummydata/configuration/dangerous_goods'
    });
});
