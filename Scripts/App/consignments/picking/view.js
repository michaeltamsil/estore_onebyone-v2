// scripts/app/storage/warehouse/detail/storage_location/
define(function(require, exports, module) {
    'use strict';
    var DefaultModule = require('defaultmodule');
    var template = require('text!./template.html');
    var Table = require('./table/table');
    var Collection = require('./collection');
    var Paging = require('paging');

    module.exports = DefaultModule.extend({
        template: _.template(template),
        initialize: function(){
            this.table = new Table({
                collection: new Collection()
            });

            this.paging = new Paging({
                collection: this.table.collection
            });

            this.on('cleanup', function() {
                this.table.destroy();
            });
        },
        afterRender: function(){
            this.$('[obo-table]').append(this.table.el);
            this.table.render();

            this.insertView('[obo-paging]', this.paging);
            this.paging.render();

            this.table.collection.fetch();
        }
    });
});
