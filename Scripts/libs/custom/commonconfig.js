define(function(require, exports, module) {
    module.exports = {
        requestServer: window.location.hostname == 'localhost' ? 'http://localhost:50981/api/' : 'http://api.logistical.onebyone.co.id/api/',
        requestServerNotAPI: window.location.hostname == 'localhost' ? 'http://localhost:50981/' : 'http://api.logistical.onebyone.co.id/',
        cookieFields: {
            Authorization: 'Authorization',
            userName: 'userName',
            roleName: 'roleName',
            firstName: 'firstName'
        },
        datePickerFormat: 'MMM DD, YYYY',
        aryLogin: ['login', 'forgot_password', 'reset_password'],
        googleMapAddressInterest: {
            postcode: 'postal_code',
            locality: 'locality',
            city: 'administrative_area_level_2',
            state: 'administrative_area_level_1',
            country:'country'
        },
        googleMapAddressInterestIsShortName: {
            locality: true,
            city: true
        },
        emptyAddressInterest: {
            postcode: '',
            locality: '',
            city: '',
            state: '',
            country:''
        }
    }
});