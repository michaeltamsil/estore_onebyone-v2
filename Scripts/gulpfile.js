// installation
// npm install gulp gulp-concat gulp-minify-css del gulp-intercept gulp-imagemin imagemin-pngquant gulp-uglify gulp-replace gulp-jshint jshint-stylish

// gulp-minify-css change to gulp-clean-css 
// npm install gulp gulp-concat gulp-clean-css del gulp-intercept gulp-imagemin imagemin-pngquant gulp-uglify gulp-replace gulp-jshint jshint-stylish


// execute
// gulp

// D:\>gulp --gulpfile "D:/git/gptt/optipediagtt/src/app/GTT.Web/Scripts/gulpfile.js"

const gulp = require('gulp');
const uglify = require('gulp-uglify');
const concat = require('gulp-concat');
const intercept = require('gulp-intercept');
const del = require('del');
const imagemin = require('gulp-imagemin');
const pngquant = require('imagemin-pngquant');
const minifyCss = require('gulp-minify-css');
const replace = require('gulp-replace');
const jshint = require('gulp-jshint');
const stylish = require('jshint-stylish');

const folder = './';
const IndexProdDir = folder + '../';
const IndexProdFile = 'IndexProd.html';
const IndexProd = IndexProdDir + IndexProdFile;

var IndexProdReplace = function (string, newReplace) {
	var regex = /data-main="+.*?"/;
	return string.replace(regex, 'data-main="' + newReplace + '"')
};

var pad = function (number) {
	if (number < 10) {
		return '0' + number;
	}
	return number;
}

var getDateTimeISO = function (date) {
	if (!date)
		date = new Date();
	return date.getUTCFullYear() + '' +
	pad(date.getUTCMonth() + 1) + '' +
	pad(date.getUTCDate()) + '_' +
	pad(date.getUTCHours()) + '' +
	pad(date.getUTCMinutes()) + '' +
	pad(date.getUTCSeconds());
}

const versionApp = getDateTimeISO();
const distFolder =  folder + 'dist/' + versionApp +'/';
const distFolderApp = distFolder + 'app';
const distFolderLibs = distFolder + 'libs';
const distFolderLibs2 = distFolder + 'libs2';
const distFolderVendor = distFolder + 'vendor';
const distFolderCss = distFolder + 'css';
const distFolderImg = distFolder + 'img';


// Begin js optimize
gulp.task('IndexProdChangeJsPath', function () {
	return gulp.src(IndexProd)
	.pipe(intercept(function (file) {
		file.contents = new Buffer(IndexProdReplace(file.contents.toString(), '/Scripts/dist/' + versionApp + '/app/config.js'));
		return file;
	}))
	.pipe(concat(IndexProdFile))
	.pipe(gulp.dest(IndexProdDir))
});

gulp.task('delDistVersion', function () {
	return del([folder + 'dist/*']);
});

gulp.task('copyFolderLibs2',['delDistVersion'], function(){
	return gulp.src([folder + 'libs2/**/*'])
		.pipe(uglify({mangle: false}))
		.pipe(gulp.dest(distFolderLibs2));
});

gulp.task ('copyFolderLibs',['copyFolderLibs2'], function(){
	 return gulp.src([folder + 'libs/**/*'])
		.pipe(gulp.dest(distFolderLibs));
});

gulp.task ('copyFolderVendor', function(){
	 return gulp.src([folder + 'vendor/**/*'])
		.pipe(gulp.dest(distFolderVendor));
});

gulp.task('compressFolderLibsImg', ['copyFolderLibs', 'copyFolderVendor'], function(){
	return gulp.src([distFolderLibs + '/**/*.png',
	distFolderLibs + '/**/*.gif',
	distFolderLibs + '/**/*.jpg'])
		.pipe(imagemin({
			progresive: true,
			optimaztionLevel: 7,
			use: [pngquant()]
		}))
		.pipe(gulp.dest(distFolderLibs));
});

gulp.task('compressFolderLibsCss', ['copyFolderLibs'], function(){
	return gulp.src(distFolderLibs + '/**/*.css')
		.pipe(minifyCss())
		.pipe(gulp.dest(distFolderLibs));
});


gulp.task ('compressFolderLibs', ['compressFolderLibsImg', 'compressFolderLibsCss', 'copyFolderLibs'], function(){
	return gulp.src([folder + 'libs/**/*.js'])
		.pipe(uglify({mangle: false}))
		.pipe(gulp.dest(distFolderLibs));
});

gulp.task ('copyFolderApp', ['compressFolderLibs'], function(){
	return gulp.src([folder + 'app/**/*'])
		.pipe(gulp.dest(distFolderApp));
});

gulp.task('replaceSrcImgFromFolderApp', ['IndexProdChangeJsPath', 'copyFolderApp', 'compressFolderImg'], function(){
	return gulp.src([distFolderApp + '/**/*.js', distFolderApp + '/**/*.html'])
	.pipe(replace(/\/Scripts\/img\//g,'/Scripts/dist/' + versionApp + '/img/'))
	.pipe(gulp.dest(distFolderApp));
});


gulp.task('compressFolderAppJs',['replaceSrcImgFromFolderApp'], function(){
	return gulp.src([distFolderApp + '/**/*.js'])
	.pipe(uglify({mangle: false}))
	.pipe(gulp.dest(distFolderApp));
});
//End js optimize


// Begin css optimize
gulp.task('copyFolderImg', function(){
	return gulp.src(folder + 'img/**/*')
		.pipe(gulp.dest(distFolderImg));
});

gulp.task('compressFolderImg', ['copyFolderImg'], function(){
	return gulp.src([distFolderImg + '/**/*.png',
	distFolderImg + '/**/*.gif',
	distFolderImg + '/**/*.jpg'])
		.pipe(imagemin({
			progresive: true,
			optimaztionLevel: 7,
			use: [pngquant()]
		}))
		.pipe(gulp.dest(distFolderImg));
});

gulp.task('copyFolderCss',function(){
	return gulp.src([folder + 'css/*.css'])
		.pipe(gulp.dest(distFolderCss));
});

gulp.task('compressFolderCss', ['copyFolderCss', 'compressFolderImg'], function(){
	return gulp.src(distFolderCss + '/**/*.css')
	.pipe(minifyCss())
	.pipe(gulp.dest(distFolderCss));
});
// End css optimize


gulp.task('hint', function() {
  return gulp.src([folder + 'app/**/*.js', folder + 'libs2/**/*.js'])
    .pipe(jshint({
		multistr: true,
		laxcomma: true,
		undef: true,
		unused: true,
		predef: ['_', 'define', 'window', '$', 'Backbone', 'require', 'module', 'exports', 'requirejs', 'document', 'navigator','FormData','XMLHttpRequest', 'Option', 'alert', 'setTimeout', 'event', 'FileReader', 'google', 'setInterval', 'clearInterval' ]
	}))
    .pipe(jshint.reporter(stylish));
});

gulp.task('default', ['compressFolderAppJs', 'compressFolderCss']);
//gulp.task('default', ['IndexProdChangeJsPath']);
//gulp.task('default', ['compressFolderAppJs']);