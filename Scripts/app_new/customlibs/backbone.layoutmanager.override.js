define(function(require, exports, module) {
    'use strict';

    const LayoutManager = require('layoutmanager.original');
    const moment = require('moment');

    module.exports = LayoutManager.extend({
        serialize: function() {
            const commonFunction = require('commonfunction');
            var data = LayoutManager.prototype.serialize.call(this);
            data = _.extend({}, data, {
                _getCurrentHash: commonFunction.getCurrentHash,
                _getCurrentHashToLevel: commonFunction.getCurrentHashToLevel,
                _getCurrentHashOmitSuffix: commonFunction.getCurrentHashOmitSuffix,
                moment: moment
            });
            return data;
        }

    });
});
