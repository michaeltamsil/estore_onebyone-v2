define(function(require, exports, module) {
    'use strict';
    var Backbone = require('backbone');
    var commonFunction = require('commonfunction');

    module.exports = Backbone.Collection.extend({
        initialize: function(options) {
            var self = this;
            var _self = this;
            if (this.beforeInitialize) {
                this.beforeInitialize(options);
            }

            this.listenTo(this, 'error', function(collection, xhr) {
                commonFunction.responseStatusNot200({
                    'xhr': xhr
                });
            });

            this.parameters = {
                DisplayRows: 10,
                Page: 1,
                OrderBy: '',
                FilterParams: {}
            };

            this.paramPaging = {
                DisplayRows: 0,
                Page: 0,
                TotalPage: 0
            };

            _.each(this.paramPaging, function(value, index) {
                this.paramPaging[index] = this.parameters[index];
            }, this);
        },
        parse: function(data) {
            if (data) {
                this.parameters.Page = data.Page;
                this.parameters.DisplayRowPage = data.DisplayRowPage;
            }

            _.each(['DisplayRows', 'Page', 'TotalPage', 'OrderBy', 'TotalRows'], function(value) {
                if (data[value] != undefined)
                    this.paramPaging[value] = data[value];
            }, this);
            return data.ResultList;
        },
        fetch: function(options) {
            options = options || {};
            this.parameters.Page = this.paramPaging.Page;
            options.data = _.extend({}, this.parameters, options.data);

            return Backbone.Collection.prototype.fetch.call(this, options);
        },
        setOrderBy: function() {
            this.parameters.OrderBy = this.paramPaging.SortField + '-' + this.paramPaging.SortOrder;
        },
        SetOrderByEmpty: function() {
            this.paramPaging.SortField = '';
            this.paramPaging.SortOrder = '';
            this.parameters.OrderBy = '';
        },
        setParametersFilterParamsEmpty: function() {
            this.SetOrderByEmpty();
            this.parameters.FilterParams = {};
            this.setPageNo1();
            this.trigger('changeViewSorting');
        },
        setParamPagingSortFieldSortOrder: function(SortField, SortOrder) {
            this.paramPaging.SortField = SortField;
            this.paramPaging.SortOrder = SortOrder;
            this.setPageNo1();
            this.setOrderBy();
            this.trigger('changeViewSorting');
        },
        setPageNo1: function() {
            this.paramPaging.Page = 1;
        }
    });
});
