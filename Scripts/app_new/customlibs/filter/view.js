// filter
define(function(require, exports, module) {
    'use strict';
    var LayoutManager = require('layoutmanager');
    var commonFunction = require('commonfunction');

    module.exports = LayoutManager.extend({
        className: 'card-block',
        // begin example
        // template: _.template(template),
        // efaultShowFields example
        // defaultShowFields : ['CustomerNumber', 'AccountOwner.AccountNo', 'AccountOnwer.AccountOwnerID', 'ListCustomerTag', 'IsActive'],
        // end example
        numberFieldsShowed: 5,
        initialize: function() {
            // begin example
            // this.fieldsShowed = this.defaultShowFields.slice();
            //end example

            // commonFunction.setSelect2CustomerAccountOwner(this, {
            //     selector: '[name="AccountOnwer.AccountOwnerID"]',
            //     placeholder: 'Account Owner'
            // });
            //
            // commonFunction.setSelect2CustomerTags(this, {
            //     selector: '[name="ListCustomerTag"]',
            //     placeholder: 'Tag'
            // });
            // this.on('beforeRender remove cleanup', function() {
            //     this.$('[name="IsActive"]').select2('destroy');
            // });

        },
        events: {
            'change input[type="text"]': 'doFilterAndSorting',
            'change select': 'changeSelect',
            'click [obo-clearfilter]': 'clearFilter'
        },
        afterRender: function() {
            this.showHIdeFieldsFilter(this.defaultShowFields);
        },
        showFilterFields: function() {
            this.showHIdeFieldsFilter(this.fieldsShowed);
            this.setHiddenInputToEmpty();
            this.collection.setPageNo1();
            this.addFilter();
        },
        changeSelect: function(e) {
            var dom = $(e.currentTarget);
            if (dom.val()) {
                this.setChangeSort(dom);
                this.addFilter();
            }
        },
        doFilterAndSorting: function(e) {
            if (e.currentTarget) {
                this.setChangeSort($(e.currentTarget));
                this.addFilter();
            }
        },
        clearFilter: function(e) {
            this.$("select.select2-hidden-accessible").val('').trigger('change');
            this.$('input[type="text"]').val('');
            this.setHiddenInputToEmpty();
            this.collection.setParametersFilterParamsEmpty();
            this.collection.fetch();
        },
        setFilterToDefault: function() {
            this.fieldsShowed = this.defaultShowFields;
            this.$("select.select2-hidden-accessible").val('').trigger('change');
            this.$('input[type="text"]').val('');
            this.setHiddenInputToEmpty();
            this.collection.setParametersFilterParamsEmpty();
            this.showFilterFields();
        },
        setHiddenInputToEmpty: function() {
            _.each(this.$('[data-filter-name].hide'), function(item) {
                $('input[type="text"]', item).prop('value', '');
            }, this);
        },
        showHIdeFieldsFilter: function(fields) {
            this.$('[data-filter-name]').addClass('hide');
            _.each(fields, function(item) {
                this.$('[data-filter-name="' + item + '"]').removeClass('hide');
            }, this)
            this.afterShowHideFieldsFilter && this.afterShowHideFieldsFilter();
        },
        addFilter: function() {
            var filterParams = [];
            _.each(this.$('[data-filter-name]:not(.hide) input[type="text"]'), function(item) {
                if ($(item).val() != '') {
                    var val = $(item).val();
                    if ($(item).attr('bootstrap-datetimepicker') && $(item).attr('bootstrap-datetimepicker').length) {
                        val = $(item).data('DateTimePicker').date().format('YYYYMMDD');
                    }

                    filterParams.push({
                        Key: $(item).attr('name'),
                        Value: val
                    });
                }
            });

            _.each(this.$('[data-filter-name]:not(.hide) select'), function(item) {
                if ( !($(item).val() == '' || $(item).val() == undefined) ) {
                    var dom = $(item)[0];
                    filterParams.push({
                        Key: $(item).attr('name'),
                        Value: dom.options[dom.selectedIndex][$(item).attr('getbyvalueonly') ? 'value' : 'text']
                    });
                }
            });

            this.collection.parameters.FilterParams = filterParams;
            this.collection.fetch();
        },
        setChangeSort: function(dom) {
            this.collection.setParamPagingSortFieldSortOrder(dom.attr('name'), 'asc');
            this.trigger('changeViewSorting');
        }
    });
});
