define(function(require, exports, module) {
    'use strict';

    var Model = require('backbone.model');

    module.exports = Model.extend({
        idAttribute: 'CustomerID',
        urlRoot: 'Customer',
        defaults: function() {
            return {
              CustomerNo: '',
              CustomerName: '',
              CustomerTradingAs: '',
              AccountOwner: {
                AccountNo : ''
              },
              CustomerABN:'',
              CustomerACN: '',
              Currency:'',
              AccountNo:'',
              DateStart:'',
              ListCustomerTag :'',
              Phone: '',
              Mobile: '',
              Fax: '',
              CreditLimit: '',
              InvoiceEmail: '',
              FreightPayable : {
                FreightPayableName:''
              },
              PodEmail:'',
              IsActive:'',
              PaymentTerm:{
                  PaymentTermID: '',
                  PaymentTermName:''
              }
            }
        }
    });
});
