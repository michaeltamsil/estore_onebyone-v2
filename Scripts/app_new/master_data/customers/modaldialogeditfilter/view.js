define(function(require, exports, module) {
    'use strict';

    const View = require('modaldialogeditfilter');
    const template = require('text!./template.html');

    module.exports = View.extend({
        template: _.template(template),
    });
});
