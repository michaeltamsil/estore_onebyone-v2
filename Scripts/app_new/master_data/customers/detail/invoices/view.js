// scripts/app/customers/detail/invoices
define(function(require, exports, module) {
    'use strict';
    var LayoutManager = require('layoutmanager');
    var template = require('text!./template.html');
    var commonConfig = require('commonconfig');
    require('select2');
    require('datetimepicker');

    module.exports = LayoutManager.extend({
        className: '',
        template: _.template(template),
        afterRender: function(){
          this.$('[name="status"]').select2();
          this.$('[date]').datetimepicker({
              defaultDate: new Date(),
              format: commonConfig.datePickerFormat
          });
        }
    });
});
