// scripts/app/customers/modaldialogeditfilter
define(function(require, exports, module) {
    'use strict';
    var LayoutManager = require('layoutmanager');
    var template = require('text!./template.html');

    module.exports = LayoutManager.extend({
        className: 'modal fade',
        template: _.template(template),
        attributes: {
            tabindex: '-1',
            role: 'dialog',
            'aria-labelledby': 'modalDialogEditFilter',
            'aria-hidden': 'true'
        }
    });
});
