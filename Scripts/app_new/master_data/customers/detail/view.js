define(function(require, exports, module) {
    'use strict';

    const LayoutManager = require('layoutmanager');
    const template = require('text!./template.html');
    let eventAggregator = require('eventaggregator');

    module.exports = LayoutManager.extend({
        name: 'customers.detail',
        template: _.template(template),
        initialize() {
            const View = require('./menu/view')
            this.insertView('header', new View())
        },
        afterRender() {
            eventAggregator.trigger('getCustomerModel', function(model) {
                self.$('.header > div > [name="CustomerName"]').text(model.toJSON().CustomerName);
            });
        }
    });
});