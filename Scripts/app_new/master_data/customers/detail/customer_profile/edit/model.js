// scripts/app/customers/detail/customer_profile/edit
define(function(require, exports, module) {
    'use strict';
    var Model = require('backbone.model');

    module.exports = Model.extend({
        urlRoot: 'Customer',
        defaults: function(){
            return {
                AccountNo: '',
                CustomerName: '',
                CustomerNo: '',
                CustomerTradingAs: '',
                CustomerABN: '',
                CustomerACN: '',
                DateStart: '',
                AccountOwnerID: '',
                PaymentTermID: '1010',
                Phone: '',
                Fax: '',
                Mobile: '',
                InvoiceEmail: '',
                PodEmail: '',
                FreightPayableBy: '',
                CreditLimit: '',
                InsertDate: '',
                FreightPayable
            }
        }
    });
});
