// scripts/app/customers/detail/addresses/add/addAnotherSocialMedia
define(function(require, exports, module) {
    'use strict';
    var LayoutManager = require('layoutmanager');
    var template = require('text!./template.html');
    var commonFunction = require('commonfunction');
    var eventAggregator = require('eventaggregator');

    module.exports = LayoutManager.extend({
        template: _.template(template),
        initialize: function() {
            var self = this;

            this.listenTo(eventAggregator, 'customers/detail/contacts/add/view', function(fn) {
                fn({
                    SocialMediaID: self.$('[socialmedia] select').val(),
                    SocialMediaUrl: self.$('[socialmedia] input').val()
                });
            });

            commonFunction.setSelect2CustomerProviderSocialMedia(this).then(function(object) {
                object.view.stopListening(object.collection, 'sync');
            });
        },
        events: {
            'click [obo-add]': 'addAnotherSocialMedia',
            'click [obo-remove]': 'removeThisView'
        },
        afterRender: function() {
            if (this.options.showLabel) {
                this.showTitle();
            }
        },
        addAnotherSocialMedia: function(e) {
            if (e) {
                if ($('.fa-plus', e.currentTarget).length) {
                    eventAggregator.trigger('customers/detail/contacts/add/addAnotherSocialMedia');
                    $('.fa-plus', e.currentTarget).removeClass('fa-plus').addClass('fa-minus');
                    $(e.currentTarget).removeAttr('obo-add').attr('obo-remove', 'true')
                }
            }
        },
        removeThisView: function() {
            this.remove();
            eventAggregator.trigger('customers/detail/contacts/add/setButtonAddSocialMedia');
        },
        changeButtonToAdd: function() {
            this.$('button[obo-remove]').removeAttr('obo-remove');
            this.$('button').attr('obo-add', 'true');
            this.$('button > i').removeClass('fa-minus').addClass('fa-plus');
        },
        changeButtonToRemove: function(){
            this.$('button[obo-add]').removeAttr('obo-add');
            this.$('button').attr('obo-remove', 'true');
            this.$('button > i').removeClass('fa-plus').addClass('fa-remove');
        },
        showTitle: function() {
            this.$('[sosmed-title]').removeClass('hide');
        }
    });
});
