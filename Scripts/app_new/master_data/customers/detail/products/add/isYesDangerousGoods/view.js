// scripts/app/customers/add
define(function(require, exports, module) {
    'use strict';
    var LayoutManager = require('layoutmanager');
    var template = require('text!./template.html');
    var commonFunction = require('commonfunction');
    var commonConfig = require('commonconfig');
    var eventAggregator = require('eventaggregator');

    module.exports = LayoutManager.extend({
        template: _.template(template),
        initialize: function(){
          commonFunction.setSelect2ProductSubRisk(this);
          commonFunction.setSelect2ProductUNNumber(this);
          commonFunction.setSelect2ProductHazchem(this);
          commonFunction.setSelect2ProductChemical(this);
          commonFunction.setSelect2ProductClassProduct(this);
          this.listenTo(eventAggregator, 'products/add/view', function (fn) {
            var data = commonFunction.formDataToJson(this.$('form').serializeArray());
            fn({
              'DangerousGoods': data
            });
          });
        }
    });
});
