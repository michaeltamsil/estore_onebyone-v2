// scripts/app/customers/add
define(function(require, exports, module) {
    'use strict';
    var LayoutManager = require('layoutmanager');
    var template = require('text!./template.html');
    var commonFunction = require('commonfunction');
    var commonConfig = require('commonconfig');
    var Model = require('./../../../model');
    var ModelPut = require('./model');
    var Cookies = require('Cookies');
    var ladda = require('ladda.jquery');
    var eventAggregator = require('eventaggregator');
    require('select2');
    require('datetimepicker');
    require('moment');
    require('tagsinput');
    require('maxlength');
    var UrlRoot = commonConfig.requestServer;
    var authorization = Cookies.get(commonConfig.cookieFields.Authorization);

    module.exports = LayoutManager.extend({
        className: 'container-fluid main-content',
        template: _.template(template),
        initialize: function() {
            var self = this;
            this.ladda = {};
            this.model = new Model();
            this.modelPut = new ModelPut();

            this.model.set(this.model.idAttribute, commonFunction.getUrlHashSplit(3));
            this.listenToOnce(this.model, 'sync', function(model) {
                this.render();
                var data = model.toJSON();

                commonFunction.setSelect2ProductSKUType(this).then(function(object) {
                    self.$(object.options.selector).val(data.SKUType.SKUTypeID).trigger('change');


                    self.changeSKUType(data);
                });;
                commonFunction.setSelect2ProductProductCategory(this, {
                    selector: '[name="ProductCategoryID"]'
                }).then(function(object) {
                    self.$(object.options.selector).val(data.ProductCategory.ProductCategoryID).trigger('change');
                });
                commonFunction.setSelect2ProductManufactureLeadTime(this).then(function(object) {
                    self.$(object.options.selector).val(data.ManufactureLeadTime).trigger('change');
                });
                commonFunction.setSelect2StorageChargePer(this).then(function(object) {
                    self.$(object.options.selector).val(data.StorageChargePer.StorageChargePerID).trigger('change');
                });
                commonFunction.setSelect2Status(this).then(function(object) {
                    var isActive = data.IsActive;
                    self.$(object.options.selector).val(isActive.toString()).trigger('change');
                });

                this.listenTo(this.model, 'sync', function(model) {
                    Backbone.history.navigate('products', true);
                });

            }, this);
            this.once('afterRender', function() {
                this.model.fetch();
            });
            this.listenTo(this.model, 'sync error', function() {
                self.$('[obo-dosave]').removeAttr('disabled', 'disabled').html('Save');
                // self.ladda['obo-dosave'].ladda('stop');
            });
            this.listenTo(this.model, 'request', function() {
                // self.$('button[type=submit]').ladda('bind', {
                //     timeout: 2000
                // });
            });
        },
        events: {
            'click [obo-dosave]': 'doSave',
            'change [name="IsDangerousGoods"]': 'changeRadioDangerousGoods',
            'change [name="IsTemperatureSensitive"]': 'changeTemperatureSensitive',
            'change [name="SKUTypeID"]': 'changeSKUType',
            'change [name="Length"]':'calculateCubic',
            'change [name="Width"]':'calculateCubic',
            'change [name="Height"]':'calculateCubic'
        },
        afterRender: function() {
            $('[name="ProductCode"]').maxlength({
                max: 50,
                feedbackText: 'Text limited to {m} characters. Remaining: {r}'
            });
            $('[name="ProductDescription"]').maxlength({
                max: 500,
                feedbackText: 'Character limited to {m} characters. Remaining: {r}'
            });
            this.$('[date]').datetimepicker({
                defaultDate: new Date(),
                format: commonConfig.datePickerFormat,
            });
            var authorization = Cookies.get(commonConfig.cookieFields.Authorization);
            this.$('[name="CustomerID"]').select2({
                minimumInputLength: 1,
                ajax: {
                    headers: {
                        'Authorization': authorization
                    },
                    url: commonConfig.requestServer + 'Customer',
                    dataType: 'json',
                    type: "GET",
                    quietMillis: 50,
                    data: function(params) {
                        var queryParameters = {
                            DisplayRows: 100,
                            Page: 1,
                            OrderBy: '',
                            FilterParams: [{
                                Key: 'CustomerName',
                                Value: params.term
                            }]
                        }
                        return queryParameters;
                    },
                    processResults: function(data) {
                        return {
                            results: $.map(data.ResultList, function(item) {
                                return {
                                    text: item.CustomerName,
                                    id: item.CustomerID
                                }
                            })
                        };
                    }
                }
            });
            var data = this.model.toJSON();
            this.$('[name="IsDangerousGoods"][value="'+ data.IsDangerousGoods +'"]').prop('checked',true);
            this.$('[name="FragileGoods"][value="'+ data.IsFragileGoods +'"]').prop('checked',true);
            this.$('[name="PerishableGoods"][value="'+ data.IsPerishableGoods +'"]').prop('checked',true);
            this.$('[name="IsTemperatureSensitive"][value="'+ data.IsTemperatureSensitive +'"]').prop('checked',true);
            if (data.IsDangerousGoods){
                var model = new Backbone.Model();
                model.set(data);
                this.changeRadioDangerousGoods({
                    model: model
                });
            }else{
                this.changeRadioDangerousGoods();
            }

            if (data.IsTemperatureSensitive){
                var model = new Backbone.Model();
                model.set(data);
                this.changeTemperatureSensitive({
                    model: model
                });
            }else{
                this.changeTemperatureSensitive();
            }
            this.calculateCubic();
        },
        calculateCubic : function(){
          var l = this.$('[name="Length"]').val();
          var w = this.$('[name="Width"]').val();
          var h = this.$('[name="Height"]').val();
          this.$('[name="CubicMetres"]').val(l*w*h);
        },
        changeSKUType: function(data) {
            var value = this.$('[name="SKUTypeID"]').select2('data')[0].text;
            var realData = (data && !data.currentTarget) ? data : undefined;
            this.renderProductpactUnit(value, realData);
            this.renderStorageChargeUnit(value, realData);
            this.renderTransportChargeUnit(value, realData);
            var attribute = this.$('[name="ItemQuantity"], [name="ItemPerInnerQuantity"], [name="ItemPerCartonQuantity"], [name="QuantityPalletPerCarton"]');
            this.$(attribute).addClass('text-muted');
            attribute.prop('disabled', true);
            switch (value) {
                case 'Carton':
                    var attribute = this.$('[name="ItemPerCartonQuantity"],[name="QuantityPalletPerCarton"]');
                    this.$(attribute).removeClass('text-muted');
                    attribute.prop('disabled', false);

                    break;
                case 'Inner':
                    var attribute = this.$('[name="ItemPerInnerQuantity"],[name="ItemPerCartonQuantity"],[name="QuantityPalletPerCarton"]');
                    this.$(attribute).removeClass('text-muted');
                    attribute.prop('disabled', false);
                    break;
                case 'Item':
                    var attribute = this.$('[name="ItemQuantity"],[name="ItemPerInnerQuantity"],[name="ItemPerCartonQuantity"],[name="QuantityPalletPerCarton"]');
                    this.$(attribute).removeClass('text-muted');
                    attribute.prop('disabled', false);

                    break;
                case 'Pallet':
                    var attribute = this.$('[name="QuantityPalletPerCarton"]');
                    this.$(attribute).removeClass('text-muted');
                    attribute.prop('disabled', false);
                    break;
                default:

            }
        },
        renderProductpactUnit: function(value, defaultData) {
            var self = this;
            $.ajax({
                url: UrlRoot + "Product/ProductPackUnit?SKUType=" + value,
                headers: {
                    'Authorization': authorization
                },
                success: function(data) {
                    var option = new Option('Select Product Pack Unit');
                    $(option).attr('selected', 'selected').attr('disabled', 'disabled');
                    self.$('[name="ProductPackUnit"]').empty().append(option);
                    _.each(data, function(item) {
                        self.$('[name="ProductPackUnit"]').append(new Option(item.ProductPackUnitName, item.ProductPackUnitID));
                    });
                    self.$('[name="ProductPackUnit"]').prop('disabled', false);
                    self.$('[name="ProductPackUnit"]').select2();
                    if(defaultData){
                        self.$('[name="ProductPackUnit"]').val(defaultData.ProductPackUnit.ProductPackUnitID).trigger('change');
                    }
                }
            });
        },
        renderStorageChargeUnit: function(value, defaultData) {
            var self = this;
            $.ajax({
                url: UrlRoot + "Product/StorageChargeUnit?SKUType=" + value,
                headers: {
                    'Authorization': authorization
                },
                success: function(data) {
                    var option = new Option('Select Storage Charge Unit');
                    $(option).attr('selected', 'selected').attr('disabled', 'disabled');
                    self.$('[name="StorageChargeUnit"]').empty().append(option);
                    _.each(data, function(item) {
                        self.$('[name="StorageChargeUnit"]').append(new Option(item.StorageChargeUnitName, item.StorageChargeUnitID));
                    });
                    self.$('[name="StorageChargeUnit"]').prop('disabled', false);
                    self.$('[name="StorageChargeUnit"]').select2();
                    if(defaultData){
                        self.$('[name="StorageChargeUnit"]').val(defaultData.StorageChargeUnit.StorageChargeUnitID).trigger('change');
                    }
                }
            });
        },
        renderTransportChargeUnit: function(value, defaultData) {
            var self = this;
            $.ajax({
                url: UrlRoot + "Product/TransportChargeUnit?SKUType=" + value,
                headers: {
                    'Authorization': authorization
                },
                success: function(data) {
                    var option = new Option('Select Transport Charge Unit');
                    $(option).attr('selected', 'selected').attr('disabled', 'disabled');
                    self.$('[name="TransportChargeUnit"]').empty().append(option);
                    _.each(data, function(item) {
                        self.$('[name="TransportChargeUnit"]').append(new Option(item.TransportChargeUnitName, item.TransportChargeUnitID));
                    });
                    self.$('[name="TransportChargeUnit"]').prop('disabled', false);
                    self.$('[name="TransportChargeUnit"]').select2();
                    if(defaultData){
                        self.$('[name="TransportChargeUnit"]').val(defaultData.TransportChargeUnit.TransportChargeUnitID).trigger('change');
                    }
                }
            });
        },
        changeRadioDangerousGoods: function(options) {
            var self = this;
            var value = this.$('[name="IsDangerousGoods"]:checked').val();
            if (value == 'true') {
                require(['./isYesDangerousGoods/view'], function(View) {
                    var view = new View(options);
                    self.removeView('[obo-view="IsYesDangerousGoods"]');
                    self.insertView('[obo-view="IsYesDangerousGoods"]', view);
                    view.render();
                });
            } else {
                self.removeView('[obo-view="IsYesDangerousGoods"]');
            }
        },
        changeTemperatureSensitive: function(options) {
            var self = this;
            var value = this.$('[name="IsTemperatureSensitive"]:checked').val();
            if (value == 'true') {
                require(['./isYesTemperatureSensitive/view'], function(View) {
                    var view = new View(options);
                    self.removeView('[obo-view="IsYesTemperatureSensitive"]');
                    self.insertView('[obo-view="IsYesTemperatureSensitive"]', view);
                    view.render();
                });
            } else {
                self.removeView('[obo-view="IsYesTemperatureSensitive"]');
            }
        },
        doSave: function(e) {
            var data = {};
            if (e && e.currentTarget) {
                $(e.currentTarget).attr('disabled', 'disabled').html('Loading...');
            }
            var data = commonFunction.formDataToJson(this.$('form').serializeArray());
            eventAggregator.trigger('products/add/view', function(obj) {
                $.extend(data, obj);
            });
            if(this.$('[name="file"]').length != 0){
              data.Attachment = this.$('[name="file"]')[0].files[0];
            }
            data.IsDangerousGoods = this.$('[name="IsDangerousGoods"]:checked').val();
            data.IsTemperatureSensitive = this.$('[name="IsTemperatureSensitive"]:checked').val();

            this.modelPut.save({
                data:data,
                type: "PUT"
            });
            this.ladda['obo-dosave'] = this.$(e.currentTarget).ladda();
            this.ladda['obo-dosave'].ladda('start');
        }
    });
});
