// scripts/app/products
define(function(require, exports, module) {
    'use strict';
    var LayoutManager = require('layoutmanager');
    var template = require('text!./template.html');
    var commonFunction = require('commonfunction');

    module.exports = LayoutManager.extend({
        name: 'products',
        className: 'container-fluid main-content',
        template: _.template(template),
        afterRender: function() {
            var View = require('./menu/view');
            var view = new View();

            this.insertView('[obo-menu]', view);
            view.render();
        }
    });
});
