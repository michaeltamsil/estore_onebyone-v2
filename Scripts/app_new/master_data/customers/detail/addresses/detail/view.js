// scripts/app/customers/detail/addresses/detail
define(function(require, exports, module) {
    'use strict';
    var LayoutManager = require('layoutmanager');
    var template = require('text!./template.html');
    var Model = require('./../model');
    var commonFunction = require('commonfunction');

    module.exports = LayoutManager.extend({
        className: 'container-fluid',
        template: _.template(template),
        initialize: function() {
            this.model = new Model();
            this.model.url = 'Customer/' + commonFunction.getUrlHashSplit(3) + '/Address/' + commonFunction.getUrlHashSplit(6);
            this.model.set(this.model.idAttribute, commonFunction.getUrlHashSplit(6));

            this.listenToOnce(this.model, 'sync', function(model) {
                this.render();
            }, this);

            this.once('afterRender', function() {
                this.model.fetch();
            });
        }
    });
});
