// scripts/app/customers/detail/addresses/filter
define(function(require, exports, module) {
    'use strict';
    var Filter = require('filter');
    var template = require('text!./template.html');
    var commonFunction = require('commonfunction');

    module.exports = Filter.extend({
        tagName: 'form',
        className: 'row',
        template: _.template(template),
        defaultShowFields: ['AddressTypeName', 'Address1', 'Country', 'State', 'IsActive'],
        initialize: function() {
            this.fieldsShowed = this.defaultShowFields.slice();

            commonFunction.setSelect2AddressType(this, {
                selector: '[name="AddressTypeName"]',
                placeholder: 'Address Type'
            });

            commonFunction.setSelect2GeneralCountry(this, {
                selector: '[name="Country"]',
                placeholder: 'Country'
            });

            commonFunction.setSelect2Status(this, {
                selector: '[name="IsActive"]',
                placeholder: 'Status'
            });

            commonFunction.setSelect2GeneralState(this, {
                placeholder: 'State'
            });
        }
    });
});
