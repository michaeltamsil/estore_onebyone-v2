//scripts/app/storage
define(function(require, exports, module) {
    'use strict';

    const
        Backbone = require('backbone'),
        commonFunction = require('commonfunction'),
        fnSetContentView = function(pathViewFile) {
        require([pathViewFile + '/view'], function(View) {
            if (View)
                commonFunction.setContentViewWithNewModuleView(new View());
        });
    }

    require('backbone.subroute');

    module.exports = Backbone.SubRoute.extend({
        initialize() {
            this.app = {};
        },
        routes: {
            '': 'showList',
            'add': 'showAdd',
            ':id': 'redirectToDetailModule',
            ':id/*subrouter': 'redirectToDetailModule',
            '*subrouter': 'redirectToShowList'
        },
        showList() {
            fnSetContentView('.')
        },
        showAdd() {
            fnSetContentView('./add')
        },
        redirectToDetailModule(id, subrouter) {
            var self = this;

            require(['./detail/router'], function(Router){
                if (!self.app.detailRouter) {
                    self.app.detailRouter = new Router(self.prefix + '/:id', {
                        createTrailingSlashRoutes: true
                    });
                }
            });
        },
        redirectToShowList() {
            this.navigateBackbonePrototype(commonFunction.getCurrentHashToLevel(2).replace('#', ''), {
                trigger: true,
                replace: true
            });
        }
    });
});