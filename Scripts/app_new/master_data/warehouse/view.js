// scripts/app/storage/warehouse
define(function(require, exports, module) {
    'use strict';
    var DefaultModule = require('defaultmodule');
    var template = require('text!./template.html');
    var Table = require('./table/table');
    var Collection = require('./collection');
    var Filter = require('./filter/view');
    var Paging = require('paging');
    var ModalDialog = require('./modaldialogeditfilter/view');

    module.exports = DefaultModule.extend({
        className: 'jarviswidget jarviswidget-sortable',   
        template: _.template(template),
        initialize: function() {
            var self = this;
            this.table = new Table({
                collection: new Collection()
            });

            this.filter = new Filter({
                collection: this.table.collection
            });

            this.paging = new Paging({
                collection: this.table.collection
            });

            this.ModalDialog = ModalDialog;

            this.on('cleanup', function() {
                this.table.destroy();
            });
        },
        events: {
            'click [obo-defaultfilter]': 'setFilterToDefault',
            'click [obo-showeditfilters]': 'showEditFilters'
        },
        afterRender: function() {
            this.insertView('[obo-filter]', this.filter);
            this.filter.render();

            this.$('[obo-table]').append(this.table.el);
            this.table.render();

            this.insertView('[obo-paging]', this.paging);
            this.paging.render();

            this.table.collection.fetch();
        },
        setFilterToDefault: function() {
            this.filter.setFilterToDefault();
        },
        showEditFilters: function() {
            var self = this;
            require(['./modaldialogeditfilter/view'], function(ModalDialog) {
                self.modalDialog = new ModalDialog({
                    viewFilter: self.filter
                });

                $('body').append(self.modalDialog.el);
                self.modalDialog.$el.on('hidden.bs.modal', function() {
                    self.modalDialog.remove();
                });

                self.modalDialog.once('afterRender', function() {
                    self.modalDialog.$el.modal();
                });
                self.modalDialog.render();
            });
        }
    });
});
