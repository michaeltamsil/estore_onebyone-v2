// scripts/app/customers/modaldialogeditfilter
define(function(require, exports, module) {
    'use strict';
    var LayoutManager = require('layoutmanager');
    var template = require('text!./template.html');
    var commonConfig = require('commonconfig');
    var commonFunction = require('commonfunction');
    var Cookies = require('Cookies');
    var Slider = require('bootstrap-slider');
    require('jquery.panzoom');

    module.exports = LayoutManager.extend({
        el:false,
        template: _.template(template),
        initialize: function(){
            this.authorization = Cookies.get(commonConfig.cookieFields.Authorization);
        },
        events:{
            'change [slider]': 'test'
        },
        afterRender: function(){
            var self = this;
            this.$('[obo-view="LocationCode"]').html(this.model.get('LocationCode'));

            this.$('[slider]').slider({
                reversed: true
            });

            self.$('.panzoom').panzoom({
                $zoomRange: self.$('[slider]')
            });

            var url = this.model.get('LocationScetchURL');
            if (url){
                var filename = this.model.get('LocationScetchOriginalFileName');
                var xmlHTTP = new XMLHttpRequest();

                xmlHTTP.open('GET', url);
                xmlHTTP.responseType = 'arraybuffer';
                xmlHTTP.setRequestHeader('Authorization', self.authorization);

                xmlHTTP.onload = function(){

                    var blob = new Blob([this.response], {type: 'image/' + ( commonFunction.getStringExtension(filename) || 'png' ).toLowerCase() });
                    var reader = new window.FileReader();
                    reader.onloadend = function(){
                        self.$('.panzoom img').attr('src', reader.result);
                    }
                    reader.readAsDataURL(blob);
                };
                xmlHTTP.send();
            }
        },
        test: function(e){

            var valZoom = parseFloat($(e.currentTarget).val());
            this.$('.panzoom').panzoom(
                'zoom', valZoom
            );
            //debugger;
            //console.log('halo');
        }
    });
});
