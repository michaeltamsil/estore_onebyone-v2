// scripts/app/storage/warehouse/detail/storage_location/detail/storage_location_information/edit/
define(function(require, exports, module) {
    'use strict';
    require('backbone.model.file.upload');
    var commonFunction = require('commonfunction');

    module.exports = Backbone.ModelFileUpload.extend({
        idAttribute: 'StorageLocationID',
        fileAttribute: 'Files',
        forceFileMethod: true,
        initialize: function() {
            this.urlRoot = 'Storage/' + commonFunction.getUrlHashSplit(4) + '/StorageLocation/' + commonFunction.getUrlHashSplit(7);
            this.on('error', function(model, xhr) {
                require(['commonfunction'], function(commonFunction) {
                    commonFunction.responseStatusNot200({
                        'xhr': xhr
                    });
                });
            });
        },
        defaults: function(){
            return {
                Building: '',
                Isle: '',
                Bay: '',
                Tier: '',
                LocationCode: '',
                MaxLength: 0,
                MaxWidth: 0,
                MaxHeight: 0,
                MaxWeightKgs: 0,
                MaxQuantity: 0

            }
        },
        save: function(attrs, options) {
            options = options || {};
            options.useThisUrl = commonFunction.requestServerNotAPI() + this.urlRoot;
            Backbone.ModelFileUpload.prototype.save.call(this, attrs, options);
        }
    });
});
