// scripts/app/customers/detail/contacts
define(function(require, exports, module) {
    'use strict';
    var LayoutManager = require('layoutmanager');
    var template = require('text!./template.html');
    var Model = require('./../model');
    var commonFunction = require('commonfunction');

    module.exports = LayoutManager.extend({
        className: 'container-fluid',
        template: _.template(template),
        initialize: function() {
            this.model = new Model();
            this.model.urlRoot = 'Storage/Warehouse/'+ commonFunction.getUrlHashSplit(4) +'/Location/'+ commonFunction.getUrlHashSplit(7) +'/Storagetransfers';
            this.model.set(this.model.idAttribute, commonFunction.getUrlHashSplit(10) + '.json');

            this.listenToOnce(this.model, 'sync', function(model) {
                this.render();
            }, this);

            this.once('afterRender', function() {
                this.model.fetch();
            });
        }
    });
});
