// scripts/app/customers/detail/contacts/add
define(function(require, exports, module) {
    'use strict';
    var LayoutManager = require('layoutmanager');
    var template = require('text!./template.html');
    var commonFunction = require('commonfunction');
    var commonConfig = require('commonconfig');
    var Model = require('./model');
    var ladda = require('ladda.jquery');
    var eventAggregator = require('eventaggregator');
    var Cookies = require('Cookies');
    var authorization = Cookies.get(commonConfig.cookieFields.Authorization);
    require('select2');
    require('datetimepicker');
    require('bootstrap-validator');

    module.exports = LayoutManager.extend({
        className: 'container-fluid',
        template: _.template(template),
        initialize: function() {
            var self = this;
            this.ladda = {};
            this.once('afterRender', function() {
                eventAggregator.trigger('getStorageWarehouseStorageLocationModel', function(model) {
                    self.model.set(model.toJSON());
                    debugger;
                    self.render();
                });
            });

            this.model = new Model(this.model.defaults());

            this.listenTo(this.model, 'request', function() {
                self.$('[obo-dosave]').removeAttr('disabled', 'disabled').html('SAVE');
                self.ladda['obo-dosave'].ladda('stop');
            });

            this.listenTo(this.model, 'sync error', function() {
                self.$('[obo-dosave]').removeAttr('disabled', 'disabled').html('SAVE');
                self.ladda['obo-dosave'].ladda('stop');
            });

            this.listenTo(this.model, 'sync', function(model) {
                Backbone.history.navigate(commonFunction.getCurrentHashOmitSuffix(1), true);
            });

            this.on('cleanup', function() {
                this.laddaDestroy();
            });
        },
        afterRender: function() {
            this.laddaDestroy();
            this.ladda['obo-dosave'] = this.$('[obo-dosave]').ladda();

            this.$('[name="InsertDate"]').datetimepicker({
                defaultDate: new Date(),
                format: commonConfig.datePickerFormat
            });
            this.renderValidation();
            this.renderWarehouseDestination();
            this.renderWarehouseLocationDestination();
            this.renderProduct();
        },
        renderValidation: function() {
            var self = this;
            this.$('[obo-form]').bootstrapValidator({
                    fields: {
                        TransferQuantity: {
                            validators: {
                                numeric: {
                                    message: 'The Transfer Quantity must be later than Stock Available'
                                },
                                lessThan: {
                                    value: 10,
                                    message: 'The value must be greater than or equal to 18'
                                }
                            }
                        },
                    }
                })
                .on('success.form.bv', function(e) {
                    e.preventDefault();
                    self.doSave();
                });
        },
        renderProduct: function(){
            this.$('[name="ProductID"]').select2({
                //minimumInputLength: 1,
                ajax: {
                    headers: {
                        'Authorization': authorization
                    },
                    url: commonConfig.requestServer + 'Product',
                    dataType: 'json',
                    type: "GET",
                    quietMillis: 50,
                    data: function(params) {
                        var queryParameters = {
                            DisplayRows: 100,
                            Page: 1,
                            OrderBy: '',
                            FilterParams: [{
                                Key: 'ProductName',
                                Value: (( params.term == undefined ) ? '' : params.term )
                            }]
                        }
                        return queryParameters;
                    },
                    processResults: function(data) {
                        return {
                            results: $.map(data.ResultList, function(item) {
                                return {
                                    text: item.ProductName,
                                    id: item.ProductID
                                }
                            })
                        };
                    }
                }
            });
        },

        renderWarehouseDestination : function(){
            this.$('[name="DestinationLocationID"]').select2({
                //minimumInputLength: 1,
                placeholder: 'Destination',
                ajax: {
                    headers: {
                        'Authorization': authorization
                    },
                    url: commonConfig.requestServer + 'Storage/Warehouse',
                    dataType: 'json',
                    type: "GET",
                    quietMillis: 50,
                    data: function(params) {
                        var queryParameters = {
                            DisplayRows: 100,
                            Page: 1,
                            OrderBy: '',
                            FilterParams: [{
                                Key: 'WarehouseName',
                                Value: (( params.term == undefined ) ? '' : params.term )
                            }]
                        }
                        return queryParameters;
                    },
                    processResults: function(data) {
                        return {
                            results: $.map(data.ResultList, function(item) {
                                return {
                                    text: item.WarehouseName,
                                    id: item.WarehouseID
                                }
                            })
                        };
                    }
                }
            });
        },
        renderWarehouseLocationDestination : function(){
            this.$('[name="WarehouseLocation2"]').select2({
                //minimumInputLength: 1,
                placeholder: 'Location',
                ajax: {
                    headers: {
                        'Authorization': authorization
                    },
                    url: commonConfig.requestServer + 'Storage/' + commonFunction.getUrlHashSplit(4) +'/StorageLocation',
                    dataType: 'json',
                    type: "GET",
                    quietMillis: 50,
                    data: function(params) {
                        var queryParameters = {
                            DisplayRows: 100,
                            Page: 1,
                            OrderBy: '',
                            FilterParams: [{
                                Key: 'LocationCode',
                                Value: (( params.term == undefined ) ? '' : params.term )
                            }]
                        }
                        return queryParameters;
                    },
                    processResults: function(data) {
                        return {
                            results: $.map(data.ResultList, function(item) {
                                return {
                                    text: item.LocationCode,
                                    id: item.StorageLocationID
                                }
                            })
                        };
                    }
                }
            });
        },
        doSave: function() {
            var data = {};
            var data = commonFunction.formDataToJson(this.$('form').serializeArray());
            data.InsertDate = $('[name="InsertDate"]').data('DateTimePicker').date().format('YYYYMMDD');
            // data.ProductID = commonFunction.getUrlHashSplit(3);

            data.SourceLocationID = this.$('[name="SourceLocationID"]').attr('value-locationcode');
            data.SourceStockAvailable = this.$('[name="SourceStockAvailable"]').val();
            data.DestinationStockAvailable = this.$('[name="DestinationStockAvailable"]').val();
            this.model.save(data);
            this.ladda['obo-dosave'] = this.$('[obo-dosave]').ladda();
            this.ladda['obo-dosave'].ladda('start');
        },
        laddaDestroy: function() {
            if (this.ladda['obo-dosave']) {
                this.ladda['obo-dosave'].remove();
                delete this.ladda['obo-dosave'];
            }
        }
    });
});
