define(function(require, exports, module) {
    'use strict';
    var LayoutManager = require('layoutmanager');
    var template = require('text!./template.html');
    var commonFunction = require('commonfunction');
    var commonConfig = require('commonconfig');
    var Model = require('./../../model');
    var ladda = require('ladda.jquery');
    var eventAggregator = require('eventaggregator');
    var Cookies = require('Cookies');
    var authorization = Cookies.get(commonConfig.cookieFields.Authorization);
    require('select2');
    require('datetimepicker');
    require('bootstrap-validator');

    module.exports = LayoutManager.extend({
        className: 'container-fluid',
        template: _.template(template),
        initialize: function() {
            var self = this;
            this.ladda = {};
            this.model = new Model();

            this.model.urlRoot = 'Storage/Warehouse/'+ commonFunction.getUrlHashSplit(4) +'/Location/'+ commonFunction.getUrlHashSplit(7) +'/Storagetransfers';
            this.model.set(this.model.idAttribute, commonFunction.getUrlHashSplit(10) +'.json');

            this.listenTo(this.model, 'request', function() {
                self.$('[obo-dosave]').removeAttr('disabled', 'disabled').html('SAVE');
                self.ladda['obo-dosave'].ladda('stop');
            });

            this.listenTo(this.model, 'sync error', function() {
                self.$('[obo-dosave]').removeAttr('disabled', 'disabled').html('SAVE');
                self.ladda['obo-dosave'].ladda('stop');
            });

            this.listenToOnce(this.model, 'sync', function(model) {
                this.render();
                var data = model.toJSON();
                // commonFunction.setSelect2AddressType(this).then(function() {
                //     if (data && data.AddressType && data.AddressType.AddressTypeID)
                //         self.$('[name="AddresstypeID"]').val(data.AddressType.AddressTypeID).trigger("change");
                // });
                //
                // commonFunction.setSelect2GeneralCountry(this).then(function() {
                //     self.$('[name="Country"]').val(data.CountryID).trigger("change");
                // });
                //
                // commonFunction.setSelect2Status(this, {
                //     selector: '[name="IsActive"]'
                // }).then(function() {
                //     var found = data.IsActive;
                //     self.$('[name="IsActive"]').val(found.toString()).trigger("change");
                // });

                this.listenTo(this.model, 'sync', function() {
                    Backbone.history.navigate(commonFunction.getCurrentHashOmitSuffix(1), true);
                });

            }, this);

            this.once('afterRender', function() {
                this.model.fetch();
            });

            this.on('cleanup', function() {
                this.laddaDestroy();
            });
        },
        afterRender: function() {
            if (!this.ladda['obo-dosave']) {
                this.ladda['obo-dosave'] = this.$('[obo-dosave]').ladda();
            };
            this.$('[name="Date"]').datetimepicker({
                defaultDate: new Date(),
                format: commonConfig.datePickerFormat
            });
            this.renderValidation();
            this.renderProduct()
        },
        renderValidation: function() {
            var self = this;

            this.$('[obo-form]').bootstrapValidator({
                    fields: {
                        TransferQuantity: {
                            validators: {
                                notEmpty: {},
                                numeric: {
                                    message: 'The Transfer Quantity must be a numeric number'
                                }
                            }
                        },
                    }
                })
                .on('success.form.bv', function(e) {
                    e.preventDefault();
                    self.doSave();
                });
        },
        renderProduct: function(){
            this.$('[name="ProductName"]').select2({
                //minimumInputLength: 1,
                ajax: {
                    headers: {
                        'Authorization': authorization
                    },
                    url: commonConfig.requestServer + 'Customer',
                    dataType: 'json',
                    type: "GET",
                    quietMillis: 50,
                    data: function(params) {
                        var queryParameters = {
                            DisplayRows: 100,
                            Page: 1,
                            OrderBy: '',
                            FilterParams: [{
                                Key: 'CustomerName',
                                Value: (( params.term == undefined ) ? '' : params.term )
                            }]
                        }
                        return queryParameters;
                    },
                    processResults: function(data) {
                        return {
                            results: $.map(data.ResultList, function(item) {
                                return {
                                    text: item.CustomerName,
                                    id: item.CustomerID
                                }
                            })
                        };
                    }
                }
            });
        },
        doSave: function() {
            var data = {};
            var data = commonFunction.formDataToJson(this.$('form').serializeArray());
            data.CustomerID = commonFunction.getUrlHashSplit(3);
            data.Date = this.$('[name="Date"]').data('DateTimePicker').date().format('YYYYMMDD');
            this.model.save(data);
            this.ladda['obo-dosave'] = this.$('[obo-dosave]').ladda();
            this.ladda['obo-dosave'].ladda('start');
        },
        laddaDestroy: function() {
            if (this.ladda['obo-dosave']) {
                this.ladda['obo-dosave'].remove();
                delete this.ladda['obo-dosave'];
            }
        }
    });
});
