// scripts/app/storage/warehouse/detail/storage_location/add
define(function(require, exports, module) {
    'use strict';
    var LayoutManager = require('layoutmanager');
    var template = require('text!./template.html');
    var commonFunction = require('commonfunction');
    var commonConfig = require('commonconfig');
    var Model = require('./model');
    var ladda = require('ladda.jquery');
    require('bootstrap-validator');

    module.exports = LayoutManager.extend({
        className: 'container-fluid main-content',
        template: _.template(template),
        initialize: function() {
            var self = this;
            this.ladda = {};
            this.model = new Model();

            this.listenTo(this.model, 'request', function() {
                self.$('[obo-dosave]').attr('disabled', 'disabled');
                self.ladda['obo-dosave'].ladda('start');
            });

            this.listenTo(this.model, 'sync error', function() {
                self.$('[obo-dosave]').removeAttr('disabled', 'disabled');
                self.ladda['obo-dosave'].ladda('stop');
            });

            this.listenTo(this.model, 'sync', function(model) {
                self.laddaDestroy();
                Backbone.history.navigate(commonFunction.getCurrentHashOmitSuffix('1'), true);
            });

            commonFunction.setSelect2ZoneType(this);
            commonFunction.setSelect2ZoneArea(this);
            commonFunction.setSelect2ProductSKUType(this);
        },
        events: {
            'change [name="Building"]': 'updateLocationCode',
            'change [name="Isle"]': 'updateLocationCode',
            'change [name="Bay"]': 'updateLocationCode',
            'change [name="Tier"]': 'updateLocationCode'
        },
        afterRender: function() {
            if (this.ladda['obo-dosave']) {
                this.laddaDestroy();
            }
            this.ladda['obo-dosave'] = this.$('[obo-dosave]').ladda();

            this.renderValidation();
        },
        renderValidation: function() {
            var self = this;
            this.$('[obo-form]').bootstrapValidator({
                    fields: {
                        MaxLength: {
                            validators: {
                                notEmpty: {},
                                numeric: {
                                    message: 'The Maximum Length must be a numeric number'
                                }
                            }
                        },
                        MaxWidth: {
                            validators: {
                                notEmpty: {},
                                numeric: {
                                    message: 'The Maximum Width must be a numeric number'
                                }
                            }
                        },
                        MaxHeight: {
                            validators: {
                                notEmpty: {},
                                numeric: {
                                    message: 'The Maximum Height must be a numeric number'
                                }
                            }
                        },
                        MaxWeightKgs: {
                            validators: {
                                notEmpty: {},
                                numeric: {
                                    message: 'The Maximum Weight must be a numeric number'
                                }
                            }
                        },
                        MaxQuantity: {
                            validators: {
                                notEmpty: {},
                                numeric: {
                                    message: 'The Maximum Quantity mus be a numeric number'
                                }
                            }
                        }
                    }
                })
                .on('success.form.bv', function(e) {
                    e.preventDefault();
                    self.doSave();
                });
        },
        updateLocationCode: function() {
            this.$('[name="LocationCode"]').val(this.$('[name="Building"]').val()
            + this.$('[name="Isle"]').val()
            + this.$('[name="Bay"]').val()
            + this.$('[name="Tier"]').val());
        },
        doSave: function() {
            var data = commonFunction.formDataToJson(this.$('form').serializeArray());

            if (this.$('[name="Files"]').length != 0) {
                data.Files = this.$('[name="Files"]')[0].files[0];
            }

            data.WarehouseID = commonFunction.getUrlHashSplit(4);

            data.LocationCode = this.$('[name="LocationCode"]').val();
            data.LocationStorage = this.$('[name="LocationStorage"]').val();

            this.model.save(data);
        },
        laddaDestroy: function() {
            if (this.ladda['obo-dosave']) {
                this.ladda['obo-dosave'].remove();
                delete this.ladda['obo-dosave'];
            }
        }
    });
});
