// scripts/app/storage/warehouse/detail/storage_location/add
define(function(require, exports, module) {
    'use strict';
    require('backbone.model.file.upload');
    var commonFunction = require('commonfunction');

    module.exports = Backbone.ModelFileUpload.extend({
        fileAttribute: 'Files',
        forceFileMethod: true,
        initialize: function() {
            this.urlRoot = 'Storage/' + commonFunction.getUrlHashSplit(4) + '/StorageLocation';
            this.on('error', function(model, xhr) {
                require(['commonfunction'], function(commonFunction) {
                    commonFunction.responseStatusNot200({
                        'xhr': xhr
                    });
                });
            });
        },
        save: function(attrs, options) {
            options = options || {};
            options.useThisUrl = commonFunction.requestServerNotAPI() + this.urlRoot;

            Backbone.ModelFileUpload.prototype.save.call(this, attrs, options);
        }
    });
});
