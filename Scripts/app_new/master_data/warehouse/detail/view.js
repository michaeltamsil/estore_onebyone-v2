// scripts/app/storate/warehouse/detail
define(function(require, exports, module) {
    'use strict';
    var LayoutManager = require('layoutmanager');
    var template = require('text!./template.html');
    var eventAggregator = require('eventaggregator');

    module.exports = LayoutManager.extend({
        name: 'storage.warehouse.detail',
        className: 'container-fluid main-content',
        template: _.template(template),
        initialize: function(){
            var self = this;
            this.once('afterRender', function(){
                eventAggregator.trigger('getStorageWarehouseModel', function(model){
                    self.$('[name="WarehouseName"]').text(model.get('WarehouseName'));
                    self.listenTo(model, 'change:WarehouseName', function(){
                        self.$('[name="WarehouseName"]').text(model.get('WarehouseName'));
                    })
                })
                
            })
        },
        beforeRender(){
            
        },
        afterRender: function() {
            var View = require('./menu/view');
            var view = new View();

            view.render();
            this.insertView('[obo-menu]', view);

            var oboContent = this.getView('[obo-content]');
            if (oboContent){
                this.oboContent = oboContent;
            }else{
                if (this.oboContent){
                    this.setView('[obo-content]', this.oboContent);
                }
            }
        }
    });
});
