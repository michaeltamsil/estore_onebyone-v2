// scripts/app/storage/warehouse/detail/warehouse_profile
define(function(require, exports, module) {
    'use strict';
    var LayoutManager = require('layoutmanager');
    var template = require('text!./template.html');
    var eventAggregator = require('eventaggregator');

    module.exports = LayoutManager.extend({
        className: 'container-fluid',
        template: _.template(template),
        initialize: function() {
            var self = this;

            this.model.on('sync', function() {
                self.render();
            })
            eventAggregator.trigger('getStorageWarehouseModel');
        }
    });
});
