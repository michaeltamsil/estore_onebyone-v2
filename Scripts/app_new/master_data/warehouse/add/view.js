// scripts/app/storage/warehouse/add
define(function(require, exports, module) {
    'use strict';
    var LayoutManager = require('layoutmanager');
    var template = require('text!./template.html');
    var commonFunction = require('commonfunction');
    var Model = require('./model');
    var ladda = require('ladda.jquery');
    require('maxlength');
    require('select2');
    require('jquery-ui');
    require('bootstrap-validator');

    module.exports = LayoutManager.extend({
        // className: 'container-fluid',
        className: 'jarviswidget jarviswidget-sortable',
        attributes: {
            'data-widget-colorbutton': false,
            'data-widget-editbutton': false,
            role: 'widget'
        },
        template: _.template(template),
        initialize: function() {
            var self = this;
            this.ladda = {};
            this.model = new Model();

            this.listenTo(this.model, 'request', function() {
                self.$('[obo-dosave]').attr('disabled', 'disabled');
                self.ladda['obo-dosave'].ladda('start');
            });

            this.listenTo(this.model, 'sync error', function() {
                self.$('[obo-dosave]').removeAttr('disabled', 'disabled');
                self.ladda['obo-dosave'].ladda('stop');
            });

            this.listenTo(this.model, 'sync', function(model) {
                
                Backbone.history.navigate(commonFunction.getCurrentHashOmitSuffix(1), true);
            });

            commonFunction.setSelect2CustomerAccountOwner(this, {
                selector: '[name="WarehouseOwnerID"]',
                placeholder: 'Select Warehouse Owner'
            });

            commonFunction.setSelect2GeneralCountry(this, {
                selector: '[name="CountryID"]'
            });

            commonFunction.setSelect2WarehouseManagementType(this);
            commonFunction.setSelect2WarehouseManager(this, {
                newOptionHtml: function(options, model, newOption){
                    $(newOption).attr('data-warehouseemail', model.get('WarehouseEmail'))
                    .attr('data-warehousefaxnumber', model.get('WarehouseFaxNumber'))
                    .attr('data-warehousephonenumber', model.get('WarehousePhoneNumber'));

                    return newOption;
                }
            }).then(function(object){
                self.$(object.options.selector).on('select2-selected', function(eventData){
                    debugger;
                })
            });

            this.on('cleanup', function() {
                this.laddaDestroy();
            });
        },
        events: {
            'focus [name="Address1"]': 'getAutocompleteGooglePlace1',
            'keydown [name="Address1"]': 'removeWarningMessage1',
            'focusout [name="Address1"]': 'checkAutoComplete1',
            'change [name="WarehouseManagerID"]': 'setWarehouseManagerFields'
        },
        beforeRender: function() {
            this.laddaDestroy();
        },
        afterRender: function() {
            if (!this.ladda['obo-dosave']) {
                this.ladda['obo-dosave'] = this.$('[obo-dosave]').ladda();
            }

            this.$('[name="WarehouseDescription"]').maxlength({
                max: 500,
                feedbackText: 'Text limited to {m} characters. Remaining: {r}'
            });

            this.renderValidation();
        },
        renderValidation: function() {
            var self = this;
            this.$('[obo-form]').bootstrapValidator({
                    fields: {
                        Phone: {
                            validators: {
                                regexp: {
                                    regexp: /^(?=.*[1-9].*)[0-9]{5,10}$/,
                                    message: 'The format of Phone Number is invalid'
                                }
                            }
                        },
                        Fax: {
                            validators: {
                                regexp: {
                                    regexp: /^(?=.*[1-9].*)[0-9]{5,10}$/,
                                    message: 'The format of Fax Number is invalid'
                                }
                            }
                        }
                    }
                })
                .on('success.form.bv', function(e) {
                    e.preventDefault();
                    self.doSave();
                });
        },
        removeWarningMessage1: function() {
            this.$('[obo-warningMessage1]').remove();
        },
        checkAutoComplete1: function(e) {
            var self = this;
            var dom = this.$('[name="Address1"]');
            this.removeWarningMessage1();

            var found = _.find($('.ui-autocomplete li'), function(item) {
                return dom.val() == item.textContent;
            });

            if (!found) {
                dom.val('');
                this.$('[obo-dosave]').prop("disabled", true);
                var html = $('<small class="text-danger" style="display: inline;" obo-warningMessage1>Address Not Found</small>')
                this.$('[obo-view="error1"]').append(html);
                this.setAddresses();
            } else {
                this.removeWarningMessage1();
                this.$('[obo-dosave]').prop("disabled", false);
            }
        },
        setWarehouseManagerFields: function(e){
            if (e && e.currentTarget){
                var selector = e.currentTarget;
                var data = $(selector).select2('data');
                if (data && data.length){
                    var optionHtml = data[0].element;
                    this.$('[name="WarehouseManagerPhoneNumber"]').val($(optionHtml).data('warehousephonenumber'));
                    this.$('[name="WarehouseManagerFaxNumber"]').val($(optionHtml).data('warehousefaxnumber'));
                    this.$('[name="WarehouseManagerEmailAddress"]').val($(optionHtml).data('warehouseemail'));
                }
            }
        },
        getAutocompleteGooglePlace1: function() {
            var self = this;

            this.$('[name="Address1"]').autocomplete({
                source: function(req, resp) {
                    commonFunction.getGoogleMapAutoCompleteService().getPlacePredictions({
                        input: req.term,
                        types: ['geocode'],
                        componentRestrictions: {
                            country: 'au'
                        }
                    }, function(places, status) {
                        if (status === google.maps.places.PlacesServiceStatus.OK) {
                            var _places = [];
                            for (var i = 0; i < places.length; ++i) {
                                _places.push({
                                    id: places[i].place_id,
                                    value: places[i].structured_formatting.main_text,
                                    label: places[i].structured_formatting.main_text
                                });
                            }
                            self.$('[obo-dosave]').prop("disabled", false);
                            resp(_places);
                        } else {
                            $('.ui-autocomplete').css('display', 'none');
                            var html = $('<small class="text-danger" style="display: inline;" obo-warningMessage1>Address Not Found</small>')
                            self.$('[obo-view="error1"]').append(html);
                            self.setAddresses();
                            self.$('[obo-dosave]').prop("disabled", true);
                        }
                    });
                },
                select: function(event, object) {
                    if (object && object.item && object.item.id) {
                        commonFunction.getGoogleMapPlacesPlaceService().getDetails({
                            placeId: object.item.id
                        }, function(places, status) {
                            self.setAddresses(commonFunction.getGoogleMapDetailResult(places, status));

                        });
                    } else {
                        self.setAddresses();
                    }
                }
            });
        },
        setAddresses: function(results) {
            this.$('input[name="PostCode"]').val((results && results.postcode) || '');
            this.$('input[name="State"]').val((results && results.state) || '');
            this.$('input[name="City"]').val((results && results.city) || '');

            this.$('[name="CountryID"] option').filter(function() {
                return $(this).text() == (results && results.country);
            }).prop('selected', true).trigger('change');

        },
        doSave: function(e){
            var data = commonFunction.formDataToJson(this.$('form').serializeArray());
            data.Country = this.$('[name="CountryID"]').select2('data')[0].text;

            data.PostCode = this.$('[name="PostCode"]').val();
            data.State = this.$('[name="State"]').val();
            data.City = this.$('[name="City"]').val();

            this.model.save(data);
        },
        laddaDestroy: function() {
            if (this.ladda['obo-dosave']) {
                this.ladda['obo-dosave'].remove();
                delete this.ladda['obo-dosave'];
            }
        }
    });
});
