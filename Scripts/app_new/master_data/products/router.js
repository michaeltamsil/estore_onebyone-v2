define(function(require, exports, module) {
    'use strict';

    const Backbone = require('backbone');
    const commonFunction = require('commonfunction');
    require('backbone.subroute');

    const fnSetContentView = pathViewFile => {
        require([pathViewFile + '/view'], function(View) {
            if (View)
                commonFunction.setContentViewWithNewModuleView(new View());
        });
    };

    module.exports = Backbone.SubRoute.extend({
        initialize() {
            this.app = {};
        },
        routes: {
            '': 'showList',
            'add': 'showadd',
            ':id': 'redirectToDetailModule',
            ':id/*subrouter': 'redirectToDetailModule'
        },
        showList(){
            fnSetContentView('.')
        },
        redirectToDetailModule(id, subrouter) {
            var self = this;
            require(['./detail/router'], function(Router){
                if (!self.app.detailRouter) {
                    self.app.detailRouter = new Router(self.prefix + '/:id', {
                        createTrailingSlashRoutes: true
                    });
                }
            });
        },
        showadd(){
            fnSetContentView('./add');
        }
    });
});
