// scripts/app/customers/detail/orders/detail
define(function(require, exports, module) {
    'use strict';
    var LayoutManager = require('layoutmanager');
    var template = require('text!./template.html');

    module.exports = LayoutManager.extend({
        className: 'container-fluid',
        template: _.template(template)
    });
});
