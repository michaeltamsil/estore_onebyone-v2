// scripts/app/customers/add
define(function(require, exports, module) {
    'use strict';
    var LayoutManager = require('layoutmanager');
    var template = require('text!./template.html');
    var commonFunction = require('commonfunction');
    var commonConfig = require('commonconfig');
    var Model = require('./../../../model');
    var ModelPut = require('./model');
    var Cookies = require('Cookies');
    var ladda = require('ladda.jquery');
    var eventAggregator = require('eventaggregator');
    require('select2');
    require('datetimepicker');
    require('moment');
    require('tagsinput');
    require('maxlength');
    require('bootstrap-validator');
    var UrlRoot = commonConfig.requestServer;
    var authorization = Cookies.get(commonConfig.cookieFields.Authorization);

    module.exports = LayoutManager.extend({
        // className: 'container-fluid main-content',
        className: 'jarviswidget jarviswidget-sortable',
        attributes: {
            'data-widget-colorbutton': false,
            'data-widget-editbutton': false,
            role: 'widget'
        },
        template: _.template(template),
        initialize: function() {
            var self = this;
            this.ladda = {};
            this.model = new Model();
            this.modelPut = new ModelPut();

            this.model.set(this.model.idAttribute, commonFunction.getUrlHashSplit(3));
            this.listenToOnce(this.model, 'sync', function(model) {
                var data = model.toJSON();
                this.once('afterRender', function() {
                    if (data && data.Customer && data.Customer.CustomerName) {
                        this.$('[name="CustomerID"]').append(new Option(data.Customer.CustomerName, data.Customer.CustomerID)).trigger('change');
                    }
                });

                this.render();

                commonFunction.setSelect2ProductSKUType(this).then(function(object) {
                    if (data.SKUType && data.SKUType.SKUTypeID) {
                        self.$(object.options.selector).val(data.SKUType.SKUTypeID).trigger('change');
                        self.changeSKUType(data);
                    }
                });

                commonFunction.setSelect2ProductProductCategory(this, {
                    selector: '[name="ProductCategoryID"]'
                }).then(function(object) {
                    if (data.ProductCategory && data.ProductCategory.ProductCategoryID) {
                        self.$(object.options.selector).val(data.ProductCategory.ProductCategoryID).trigger('change');
                    }
                });

                commonFunction.setSelect2StorageChargePer(this).then(function(object) {
                    if (data.StorageChargePer && data.StorageChargePer.StorageChargePerID) {
                        self.$(object.options.selector).val(data.StorageChargePer.StorageChargePerID).trigger('change');
                    }
                });

                commonFunction.setSelect2Status(this,
                {
                    selector: '[name="IsActive"]',
                }).then(function(object) {
                    var isActive = data.IsActive;
                    self.$(object.options.selector).val(isActive.toString()).trigger('change');
                });

                commonFunction.setSelect2ProductManufactureLeadTime(this).then(function(object) {
                    if (data.ManufactureLeadTime && data.ManufactureLeadTime.ManufactureLeadTimeID)
                    self.$(object.options.selector).val(data.ManufactureLeadTime.ManufactureLeadTimeID).trigger('change');
                });

                this.listenTo(this.modelPut, 'sync', function() {
                    Backbone.history.navigate('products', true);
                });

            }, this);

            this.once('afterRender', function() {
                this.model.fetch();
            });

            this.listenTo(this.model, 'request', function(){
                self.$('[obo-dosave]').attr('disabled', 'disabled');
                self.ladda['obo-dosave'] && self.ladda['obo-dosave'].ladda('start');
            });

            this.listenTo(this.modelPut, 'request', function(){
                self.$('[obo-dosave]').attr('disabled', 'disabled');
                self.ladda['obo-dosave'] && self.ladda['obo-dosave'].ladda('start');
            });

            this.listenTo(this.modelPut, 'sync error', function() {
                self.$('[obo-dosave]').removeAttr('disabled', 'disabled');
                self.ladda['obo-dosave'].ladda('stop');
            });

            this.on('clearup', function(){
                this.laddaDestroy();
            });
        },
        events: {
            'click [obo-dosave]': 'doSave',
            'change [name="IsDangerousGoods"]': 'changeRadioDangerousGoods',
            'change [name="IsTemperatureSensitive"]': 'changeTemperatureSensitive',
            'change [name="SKUTypeID"]': 'changeSKUType',
            'change [name="Length"]': 'calculateCubic',
            'change [name="Width"]': 'calculateCubic',
            'change [name="Height"]': 'calculateCubic',
            'change [name="TemperatureValueMin"]': 'changeTemperatureValueMin'
        },
        afterRender: function() {
            this.laddaDestroy();
            this.ladda['obo-dosave'] = this.$('[obo-dosave]').ladda();

            $('[name="ProductCode"]').maxlength({
                max: 50,
                feedbackText: 'Code limited to {m} characters. Remaining: {r}'
            });
            $('[name="ProductDescription"]').maxlength({
                max: 500,
                feedbackText: 'Character limited to {m} characters. Remaining: {r}'
            });
            this.$('[date]').datetimepicker({
                defaultDate: new Date(),
                format: commonConfig.datePickerFormat,
            });
            var authorization = Cookies.get(commonConfig.cookieFields.Authorization);
            this.$('[name="CustomerID"]').select2({
                minimumInputLength: 1,
                ajax: {
                    headers: {
                        'Authorization': authorization
                    },
                    url: commonConfig.requestServer + 'Customer',
                    dataType: 'json',
                    type: "GET",
                    quietMillis: 50,
                    data: function(params) {
                        var queryParameters = {
                            DisplayRows: 100,
                            Page: 1,
                            OrderBy: '',
                            FilterParams: [{
                                Key: 'CustomerName',
                                Value: params.term
                            }]
                        }
                        return queryParameters;
                    },
                    processResults: function(data) {
                        return {
                            results: $.map(data.ResultList, function(item) {
                                return {
                                    text: item.CustomerName,
                                    id: item.CustomerID
                                }
                            })
                        };
                    }
                }
            });

            var data = this.model.toJSON();
            this.$('[name="IsDangerousGoods"][value="' + data.IsDangerousGoods + '"]').prop('checked', true);
            this.$('[name="IsFragileGoods"][value="' + data.IsFragileGoods + '"]').prop('checked', true);
            this.$('[name="IsPerishableGoods"][value="' + data.IsPerishableGoods + '"]').prop('checked', true);
            this.$('[name="IsTemperatureSensitive"][value="' + data.IsTemperatureSensitive + '"]').prop('checked', true);

            if (data.IsDangerousGoods) {
                var model = new Backbone.Model();
                model.set(data);
                this.changeRadioDangerousGoods({
                    model: model
                });
            } else {
                this.changeRadioDangerousGoods();
            }

            if (data.IsTemperatureSensitive) {
                var model = new Backbone.Model();
                model.set(data);
                this.changeTemperatureSensitive({
                    model: model
                });
            } else {
                this.changeTemperatureSensitive();
            }
            this.calculateCubic();
            this.renderValidation();
        },
        changeTemperatureValueMin: function() {
            var max = this.$('[name="TemperatureValueMax"]').val();
            var min = this.$('[name="TemperatureValueMin"]').val();
            if (parseInt(min) > parseInt(max)) {
                this.$('[name="TemperatureValueMin"]').val('');
                this.$('[name="TemperatureValueMin"]').attr('placeholder', "input must be smaller  than " + max);
            }
        },
        renderValidation: function() {
            var self = this;

            this.$('[obo-form]').bootstrapValidator({
                    fields: {
                        Length:{
                            validators: {
                                notEmpty: {
                                },
                                numeric: {
                                    message: 'The Length must be a numeric number'
                                }
                            }
                        },
                        Width:{
                            validators: {
                                notEmpty: {
                                },
                                numeric: {
                                    message: 'The Width must be a numeric number'
                                }
                            }
                        },
                        Height:{
                            validators: {
                                notEmpty: {
                                },
                                numeric: {
                                    message: 'The Width must be a numeric number'
                                }
                            }
                        },
                        Weight:{
                            validators: {
                                notEmpty: {
                                },
                                numeric: {
                                    message: 'The Weight must be a numeric number'
                                }
                            }
                        },
                    }
                })
                .on('success.form.bv', function(e) {
                    e.preventDefault();
                    self.doSave();
                });
        },
        calculateCubic: function() {
            var l = this.$('[name="Length"]').val();
            var w = this.$('[name="Width"]').val();
            var h = this.$('[name="Height"]').val();
            this.$('[name="CubicMetres"]').val(l * w * h);
        },
        changeSKUType: function(data) {
            var value = this.$('[name="SKUTypeID"]').select2('data')[0].text;
            var realData = (data && !data.currentTarget) ? data : undefined;
            this.renderProductpactUnit(value, realData);
            this.renderStorageChargeUnit(value, realData);
            this.renderTransportChargeUnit(value, realData);
            var attribute = this.$('[name="ItemQuantity"], [name="ItemPerInnerQuantity"], [name="ItemPerCartonQuantity"], [name="QuantityPalletPerCarton"]');
            this.$(attribute).addClass('text-muted');
            attribute.prop('disabled', true);
            switch (value) {
                case 'Carton':
                    var attribute = this.$('[name="ItemPerCartonQuantity"],[name="QuantityPalletPerCarton"]');
                    this.$(attribute).removeClass('text-muted');
                    attribute.prop('disabled', false);

                    break;
                case 'Inner':
                    var attribute = this.$('[name="ItemPerInnerQuantity"],[name="ItemPerCartonQuantity"],[name="QuantityPalletPerCarton"]');
                    this.$(attribute).removeClass('text-muted');
                    attribute.prop('disabled', false);
                    break;
                case 'Item':
                    var attribute = this.$('[name="ItemQuantity"],[name="ItemPerInnerQuantity"],[name="ItemPerCartonQuantity"],[name="QuantityPalletPerCarton"]');
                    this.$(attribute).removeClass('text-muted');
                    attribute.prop('disabled', false);

                    break;
                case 'Pallet':
                    var attribute = this.$('[name="QuantityPalletPerCarton"]');
                    this.$(attribute).removeClass('text-muted');
                    attribute.prop('disabled', false);
                    break;
                default:

            }
        },
        renderProductpactUnit: function(value, defaultData) {
            var self = this;
            $.ajax({
                url: UrlRoot + "Product/ProductPackUnit?SKUType=" + value,
                headers: {
                    'Authorization': authorization
                },
                success: function(data) {
                    var option = new Option('Select Product Pack Unit');
                    $(option).attr('selected', 'selected').attr('disabled', 'disabled');
                    self.$('[name="ProductPackUnit"]').empty().append(option);
                    _.each(data, function(item) {
                        self.$('[name="ProductPackUnit"]').append(new Option(item.ProductPackUnitName, item.ProductPackUnitID));
                    });
                    self.$('[name="ProductPackUnit"]').prop('disabled', false);
                    self.$('[name="ProductPackUnit"]').select2();
                    if (defaultData && defaultData.ProductPackUnit && defaultData.ProductPackUnit.ProductPackUnitID) {
                        self.$('[name="ProductPackUnit"]').val(defaultData.ProductPackUnit.ProductPackUnitID).trigger('change');
                    }
                }
            });
        },
        renderStorageChargeUnit: function(value, defaultData) {
            var self = this;
            $.ajax({
                url: UrlRoot + "Product/StorageChargeUnit?SKUType=" + value,
                headers: {
                    'Authorization': authorization
                },
                success: function(data) {
                    var option = new Option('Select Storage Charge Unit');
                    $(option).attr('selected', 'selected').attr('disabled', 'disabled');
                    self.$('[name="StorageChargeUnit"]').empty().append(option);
                    _.each(data, function(item) {
                        self.$('[name="StorageChargeUnit"]').append(new Option(item.StorageChargeUnitName, item.StorageChargeUnitID));
                    });
                    self.$('[name="StorageChargeUnit"]').prop('disabled', false);
                    self.$('[name="StorageChargeUnit"]').select2();
                    if (defaultData && defaultData.StorageChargeUnit && defaultData.StorageChargeUnit.StorageChargeUnitID) {
                        self.$('[name="StorageChargeUnit"]').val(defaultData.StorageChargeUnit.StorageChargeUnitID).trigger('change');
                    }
                }
            });
        },
        renderTransportChargeUnit: function(value, defaultData) {
            var self = this;
            $.ajax({
                url: UrlRoot + "Product/TransportChargeUnit?SKUType=" + value,
                headers: {
                    'Authorization': authorization
                },
                success: function(data) {
                    var option = new Option('Select Transport Charge Unit');
                    $(option).attr('selected', 'selected').attr('disabled', 'disabled');
                    self.$('[name="TransportChargeUnit"]').empty().append(option);
                    _.each(data, function(item) {
                        self.$('[name="TransportChargeUnit"]').append(new Option(item.TransportChargeUnitName, item.TransportChargeUnitID));
                    });
                    self.$('[name="TransportChargeUnit"]').prop('disabled', false);
                    self.$('[name="TransportChargeUnit"]').select2();
                    if (defaultData && defaultData.TransportChargeUnit && defaultData.TransportChargeUnit.TransportChargeUnitID) {
                        self.$('[name="TransportChargeUnit"]').val(defaultData.TransportChargeUnit.TransportChargeUnitID).trigger('change');
                    }
                }
            });
        },
        changeRadioDangerousGoods: function(options) {
            var self = this;
            var value = this.$('[name="IsDangerousGoods"]:checked').val();
            if (value == 'true') {
                require(['./isYesDangerousGoods/view'], function(View) {
                    var view = new View(options);
                    self.removeView('[obo-view="IsYesDangerousGoods"]');
                    self.insertView('[obo-view="IsYesDangerousGoods"]', view);
                    view.render();
                    view.on('afterRender', function() {

                        self.$('[obo-form]').bootstrapValidator('addField',
                            'DangerousSubRisk', {
                                validators: {
                                    notEmpty: {}
                                }
                            },
                            'DangerousUNNumber', {
                                validators: {
                                    notEmpty: {}
                                }
                            });
                        self.$('[obo-form]').bootstrapValidator('addField',
                            'DangerousHazchem', {
                                validators: {
                                    notEmpty: {}
                                }
                            },
                            'DangerousChemicalName', {
                                validators: {
                                    notEmpty: {}
                                }
                            },
                            'ClassProduct', {
                                validators: {
                                    notEmpty: {}
                                }
                            });

                        self.$('[obo-form]').bootstrapValidator('addField',
                            'ClassProduct', {
                                validators: {
                                    notEmpty: {}
                                }
                            });

                    });
                });
            } else {
                self.removeView('[obo-view="IsYesDangerousGoods"]');
            }
        },
        changeTemperatureSensitive: function(options) {
            var self = this;
            var value = this.$('[name="IsTemperatureSensitive"]:checked').val();
            if (value == 'true') {
                require(['./isYesTemperatureSensitive/view'], function(View) {
                    var view = new View(options);
                    self.removeView('[obo-view="IsYesTemperatureSensitive"]');
                    self.insertView('[obo-view="IsYesTemperatureSensitive"]', view);
                    view.render();
                });
            } else {
                self.removeView('[obo-view="IsYesTemperatureSensitive"]');
            }
        },
        doSave: function(e) {
            var self = this;
            var data = {};

            var data = commonFunction.formDataToJson(this.$('form').serializeArray());
            eventAggregator.trigger('products/add/view', function(obj) {
                $.extend(data, obj);
            });

            if (this.$('[name="file"]').length != 0) {
                data.Attachment = this.$('[name="file"]')[0].files[0];
            }

            data.IsDangerousGoods = this.$('[name="IsDangerousGoods"]:checked').val();
            data.IsTemperatureSensitive = this.$('[name="IsTemperatureSensitive"]:checked').val();
            data.id = commonFunction.getUrlHashSplit(3);

            this.modelPut.save(data, {
                useThisUrl: commonFunction.requestServerNotAPI() + 'ProductLogistical/UpdateProduct'
            });

            // Backbone.ajax({
            //     useThisUrl: commonConfig.requestServerNotAPI + 'ProductLogistical/UpdateProduct/'+commonFunction.getUrlHashSplit(3),
            //     type: 'PUT',
            //     contentType: 'application/json',
            //     data: JSON.stringify(
            //         data
            //     ),
            //     beforeSend: function() {
            //         self.ladda['obo-dosave'].ladda('start');
            //     },
            //     success: function() {
            //         self.model.trigger('sync');
            //     },
            //     error: function() {
            //         self.model.trigger('error');
            //     }
            // });
        },
        laddaDestroy: function() {
            if (this.ladda['obo-dosave']) {
                this.ladda['obo-dosave'].remove();
                delete this.ladda['obo-dosave'];
            }
        }
    });
});
