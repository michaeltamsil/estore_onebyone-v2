define(function(require, exports, module) {
    'use strict';
    
    const LayoutManager = require('layoutmanager');
    const Model = require('backbone.model');
    const template = require('text!./template.html');
    const commonConfig = require('commonconfig');
    const commonFunction = require('commonfunction')
    const cswall = require('sweetalert')

    module.exports = LayoutManager.extend({
        id: 'header',
        tagName: 'header',
        initialize: function() {
            const NewModel = Model.extend({
                urlRoot: 'Account/Logout'
            });
            this.model = new NewModel();

            this.listenTo(this.model, 'sync', function() {
                const swal = require('sweetalert');
                swal.close();
                window.location.hash = 'login';

            });
        },
        template: _.template(template)
    });
});