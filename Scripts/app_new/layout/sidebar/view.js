define(function(require, exports, module) {
    'use strict';

    var LayoutManager = require('layoutmanager');
    var template = require('text!./template.html');

    module.exports = LayoutManager.extend({
        id: 'left-panel',
        tagName: 'aside',
        template: _.template(template),
        afterRender: function(){
        	this.setActiveMenu()
        },
        events:{
        	'click a': 'linkClicked'
        },
        setActiveMenu: function(){
        	if (this.activeMenu){
        		let linkFound = this.$(`a[href="${this.activeMenu}"]`).parent('li')
                if(linkFound){
                    linkFound.addClass('active')
                }
        	}
        },
        linkClicked: function(e){
        	const parent = $(e.currentTarget).find('.menu-item-parent')
        	if (parent.length){
        		e.preventDefault();
        	}
        	$('li').removeClass('active')
        	$(e.currentTarget).parent('li').addClass('active')
        }
    });
});
