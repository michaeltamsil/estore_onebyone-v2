define(function(require, exports, module) {
    'use strict';

    require('layoutmanager');

    //// below this will suppressWarnings when using el: false on Layoutmanager
    Backbone.Layout.configure({
        suppressWarnings: true
    });

    var Router = require('./router');
    var Startup = require('./startup');


    var startup = new Startup();
    startup.Start(function() {
        window.Router = new Router();
        window.Router.start();
    });
});
