/*
/^((?!longpolling).)*$/
*/
const pathLibs = '/Scripts/app_new/libs/';
const pathCustomLibs = '/scripts/app_new/customlibs/';

require.config({
    paths: {
        'pathCustomLibs':pathCustomLibs,
        'jquery': `${pathLibs}jquery-3.3.1.min`,
        'underscore': `${pathLibs}underscore-1.8.3.min`,
        'layoutmanager.original': `${pathLibs}backbone.layoutmanager-1.0.0.min`,
        'layoutmanager': `${pathCustomLibs}backbone.layoutmanager.override`,
        'marionette': `${pathLibs}backbone.marionette-3.5.1.min`,
        'Cookies': `${pathLibs}js.cookie-2.2.0`,
        'moment': `${pathLibs}moment-2.22.1.min`,
        'require': `${pathLibs}require-2.3.5.min`,
        'text': `${pathLibs}requirejs.text-2.0.15.min`,
        'bootstrap': `${pathLibs}bootstrap-3.3.7.min`,
        'deep-model': `${pathLibs}deep.model-0.10.4.min`,
        'sweetalert': `${pathLibs}sweetalert-2.1.0.min`,
        'backbone.radio': `${pathLibs}backbone.radio-2.0.0.min`,
        'datetimepicker': `${pathLibs}bootstrap-datetimepicker-3-v4.17.46`,
        'select2': `${pathLibs}select2-4.0.3.full`,
        'commonconfig': `${pathCustomLibs}commonconfig`,
        'commonfunction': `${pathCustomLibs}commonfunction`,
        'backbone.custom': `${pathCustomLibs}backbone.custom-1.3.3`,
        'backbone': `${pathCustomLibs}backbone.override`,
        'backbone.model': `${pathCustomLibs}backbone.model`,
        'backbone.collection': `${pathCustomLibs}backbone.collection`,
        'backbone.collection.paging': `${pathCustomLibs}backbone.collection.paging`,
        'backbone.subroute': `${pathCustomLibs}backbone.subroute-0.4.6`,
        'modaldialogeditfilter': `${pathCustomLibs}modaldialogeditfilter/view`,
        'defaultmodule': `${pathCustomLibs}defaultmodule/view`,
        'paging': `${pathCustomLibs}paging/view`,
        'sorting.button': `${pathCustomLibs}sorting.button/view`,
        'filter': `${pathCustomLibs}filter/view`,
        'app.config': `${pathCustomLibs}app.config`,
        'app': `${pathCustomLibs}app`,
        'spin': `${pathLibs}spin`,
        'ladda': `${pathLibs}ladda-1.0.0`,
        'ladda.jquery': `${pathCustomLibs}ladda.jquery-1.0.0`,
        'tagsinput': `${pathLibs}bootstrap-tagsinput-0.8.0`,
        'bootstrap-validator': `${pathCustomLibs}bootstrapValidator-0.5.2`,
        'eventaggregator': `${pathCustomLibs}eventaggregator`,
        'maxlength': `${pathLibs}jquery.maxlength-2.0.1.min`,
        'jquery.plugin':  `${pathLibs}jquery.plugin-2.0.1.min`,
        'backbone.model.file.upload': `${pathCustomLibs}backbone.model.file.upload-1.0.0`,
        'jquery-ui': `${pathLibs}jquery-ui-1.12.1.min`,
        'menu': `${pathCustomLibs}menu/view`,
    },
    shim: {
        jquery: {
            exports: 'jQuery'
        },
        tagsinput: {
            deps: ['jquery']
        },
        datetimepicker: {
            deps: ['jquery']
        },
        underscore: {
            exports: '_'
        },
        'text': {
            deps: ['require']
        },
        bootstrap: {
            deps: ['jquery'],
            exports: '$.bootstrap'
        },
        backbone: {
            deps: ['backbone.custom', 'bootstrap', 'underscore', 'text'],
            exports: 'Backbone'
        },
        'backbone.model.file.upload': {
            deps: ['backbone'],
            exports: 'Backbone'
        },
        'backbone.radio': {
            deps: ['backbone'],
            exports: 'backbone.radio'
        },
        'backbone.model':{
            deps:['backbone']
        },
        'backbone.subroute': {
            deps: ['backbone']
        },
        'layoutmanager.original': {
            deps: ['backbone'],
            exports: 'LayoutManager'
        },
        layoutmanager: {
            exports: 'layoutmanager.original'
        },
        'bootstrap-validator': {
            deps: ['jquery']
        },
        marionette: {
            deps: ['backbone.radio'],
            exports: 'Marionette'
        },
        select2: {
            deps: ['bootstrap']
        },
        'app.config':{
            deps:['jquery']
        },
        app:{
            deps:['app.config']
        },
        spin: {
            deps: ['jquery'],
            exports: 'Spinner'
        },
        ladda: {
            deps: ['spin'],
            exports: 'Ladda'
        },
        'ladda.jquery': {
            deps: ['jquery', 'ladda'],
            init: function(jquery, Ladda) {
                if (!window.Ladda)
                    window.Ladda = Ladda;
            }
        },
        'jquery.plugin': {
            deps: ['jquery']
        },
        maxlength: {
            deps: ['jquery', 'jquery.plugin']
        },
        'jquery-ui': {
            deps: ['jquery']
        }
    },
    callback: function(require) {
        requirejs(['commonfunction'], commonFunction => {
            commonFunction.checkCookieAuthorization().then(() => {
                requirejs(['main']);
            }, function() {
                window.location.hash = 'login';
                requirejs(['main']);
            });
        });
    }
});
