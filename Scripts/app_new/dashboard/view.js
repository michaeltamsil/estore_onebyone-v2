// scripts\app\customers\mainmenu
define(function(require, exports, module) {
    'use strict';
    var LayoutManager = require('layoutmanager');
    var template = require('text!./template.html');

    module.exports = LayoutManager.extend({
      className: 'container-fluid main-content',
        template: _.template(template)
    });
});
