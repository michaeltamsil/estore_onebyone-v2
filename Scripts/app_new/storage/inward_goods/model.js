define(function(require, exports, module) {
    'use strict';

    var Model = require('backbone.model');

    module.exports = Model.extend({
        idAttribute: 'InwardGoodsID',
        // urlRoot: 'storage/inward_goods'
        urlRoot: 'dummydata/storage/InwardGoods.json',
        defaults: function() {
            return {
                InwardGoodsID: '',
                ReceivedDate: '',
                Reason: '',
                Product: {
                    Productname:'',
                    ProductNumber: '',
                    ProductName: '',
                    ProductCode: '',
                    ProductCategory: '',
                },
                UnitValue: '',
                Consignor: '',
                Address1: '',
                UnitValue: '',
                Status: '',
                StorageLocation: '',
                Length: '',
                Width: '',
                Height: '',
                CubicMetres: '',
                GoodsDetails: {
                    DangerousGood:'',
                    FragileGoods:'',
                    PerishableGoods:'',
                    TemperatureSensiteve:'',
                }
            }
        }
    });
});
