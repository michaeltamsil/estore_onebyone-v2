// scripts/app/storage/add
define(function(require, exports, module) {
    'use strict';
    var LayoutManager = require('layoutmanager');
    var template = require('text!./template.html');
    var commonFunction = require('commonfunction');
    var Model = require('./model');
    var ladda = require('ladda.jquery');
    var commonConfig = require('commonconfig');
    require('maxlength');
    require('select2');
    require('jquery-ui');
    require('datetimepicker');
    require('bootstrap-validator');

    module.exports = LayoutManager.extend({
        className: 'jarviswidget jarviswidget-sortable',
        template: _.template(template),
        initialize: function() {
            var self = this;
            this.ladda = {};
            this.model = new Model();
            this.listenTo(this.model, 'request', function() {
                self.$('[obo-dosave]').attr('disabled', 'disabled');
                self.ladda['obo-dosave'].ladda('start');
            });
            this.listenTo(this.model, 'sync error', function() {
                self.$('[obo-dosave]').removeAttr('disabled', 'disabled');
                self.ladda['obo-dosave'].ladda('stop');
            });
            this.listenTo(this.model, 'sync', function(model) {
                Backbone.history.navigate(commonFunction.getCurrentHashOmitSuffix(1), true);
            });
            commonFunction.setSelect2GeneralCountry(this, {
                selector: '[name="CountryID"]'
            });
            commonFunction.setSelect2GoodsCondition(this);
            commonFunction.setSelect2SInwardGoodstatus(this);
            this.on('cleanup', function() {
                this.laddaDestroy();
            });
        },
        events: {
            // 'click [obo-dosave]': 'doSave',
            'focus [name="Address1"]': 'getAutocompleteGooglePlace1',
            'keydown [name="Address1"]': 'removeWarningMessage1',
            'focusout [name="Address1"]': 'checkAutoComplete1'
        },
        beforeRender: function() {
            this.laddaDestroy();
        },
        afterRender: function() {
            this.$('[name="ReceivedDate"]').datetimepicker({
                defaultDate: new Date(),
                format: commonConfig.datePickerFormat
            });
            this.$('select').select2();
            if (!this.ladda['obo-dosave']) {
                this.ladda['obo-dosave'] = this.$('[obo-dosave]').ladda();
            }
            this.$('[name="ProductCode"]').maxlength({
                max: 500,
                feedbackText: 'Text limited to {m} characters. Remaining: {r}'
            });
            this.renderValidation();
        },
        renderValidation: function() {
            var self = this;
            this.$('[obo-form]').bootstrapValidator({
                    fields: {
                        PhoneNumber: {
                            validators: {
                                regexp: {
                                    regexp: /^(?=.*[1-9].*)[0-9]{5,10}$/,
                                    message: 'The format of Phone Number is invalid'
                                }
                            }
                        },
                        FaxNumber: {
                            validators: {
                                regexp: {
                                    regexp: /^(?=.*[1-9].*)[0-9]{5,10}$/,
                                    message: 'The format of Fax Number is invalid'
                                }
                            }
                        }
                    }
                })
                .on('success.form.bv', function(e) {
                    e.preventDefault();
                    self.doSave();
                });
        },
        removeWarningMessage1: function() {
            this.$('[obo-warningMessage1]').remove();
        },
        checkAutoComplete1: function(e) {
            var self = this;
            var dom = this.$('[name="Address1"]');
            this.removeWarningMessage1();

            var found = _.find($('.ui-autocomplete li'), function(item) {
                return dom.val() == item.textContent;
            });

            if (!found) {
                dom.val('');
                this.$('[obo-dosave]').prop("disabled", true);
                var html = $('<small class="text-danger" style="display: inline;" obo-warningMessage1>Address Not Found</small>')
                this.$('[obo-view="error1"]').append(html);
                this.setAddresses();
            } else {
                this.removeWarningMessage1();
                this.$('[obo-dosave]').prop("disabled", false);
            }
        },
        getAutocompleteGooglePlace1: function() {
            var self = this;

            this.$('[name="Address1"]').autocomplete({
                source: function(req, resp) {
                    commonFunction.getGoogleMapAutoCompleteService().getPlacePredictions({
                        input: req.term,
                        types: ['geocode'],
                        componentRestrictions: {
                            country: 'au'
                        }
                    }, function(places, status) {
                        if (status === google.maps.places.PlacesServiceStatus.OK) {
                            var _places = [];
                            for (var i = 0; i < places.length; ++i) {
                                _places.push({
                                    id: places[i].place_id,
                                    value: places[i].structured_formatting.main_text,
                                    label: places[i].structured_formatting.main_text
                                });
                            }
                            self.$('[obo-dosave]').prop("disabled", false);
                            resp(_places);
                        } else {
                            $('.ui-autocomplete').css('display', 'none');
                            var html = $('<small class="text-danger" style="display: inline;" obo-warningMessage1>Address Not Found</small>')
                            self.$('[obo-view="error1"]').append(html);
                            self.setAddresses();
                            self.$('[obo-dosave]').prop("disabled", true);
                        }
                    });
                },
                select: function(event, object) {
                    if (object && object.item && object.item.id) {
                        commonFunction.getGoogleMapPlacesPlaceService().getDetails({
                            placeId: object.item.id
                        }, function(places, status) {
                            self.setAddresses(commonFunction.getGoogleMapDetailResult(places, status));

                        });
                    } else {
                        self.setAddresses();
                    }
                }
            });
        },
        setAddresses: function(results) {
            this.$('input[name="Postcode"]').val((results && results.postcode) || '');
            this.$('input[name="State"]').val((results && results.state) || '');
            this.$('input[name="City"]').val((results && results.city) || '');

            this.$('[name="CountryID"] option').filter(function() {
                return $(this).text() == (results && results.country);
            }).prop('selected', true).trigger('change');

        },
        doSave: function(e){
            var data = commonFunction.formDataToJson(this.$('form').serializeArray());
            data.Country = this.$('[name="CountryID"]').select2('data')[0].text;
            this.model.save(data);
        },
        laddaDestroy: function() {
            if (this.ladda['obo-dosave']) {
                this.ladda['obo-dosave'].remove();
                delete this.ladda['obo-dosave'];
            }
        }
    });
});
