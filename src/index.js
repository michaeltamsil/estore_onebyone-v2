import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
// import './../public/src/bootstrap.min.css';
// import './../public/src/dashboard.css';
// import './../public/src/font.awesome-4.7.0.min.css';
import registerServiceWorker from './registerServiceWorker';


ReactDOM.render(<App />, document.getElementById('root'));
registerServiceWorker();
