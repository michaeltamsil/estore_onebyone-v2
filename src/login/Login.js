import React, { Component } from 'react';
import './Login.css'

class Login extends Component {
    render () {
        return (
        <div>
                <div className="content">
                    <div className="col-md-4 bg-putih btn-round p-2 shadow content-2">
                        <h3>Login eStore</h3>
                        <p className="text-muted">login for redirect to dashboard</p>
                        <form className="form">
                            <input className="form-control mb-1" name="useraname" placeholder="Username" />
                            <input className="form-control mb-1" name="password" placeholder="Password" />
                            <input type="submit" className="btn btn-warning btn-block" />
                        </form>
                    </div>
                </div>
        </div>
          );
    }
}

export default Login;