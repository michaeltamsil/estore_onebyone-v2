
import React, { Component } from 'react';

class Dashboard extends Component {
    render () {
        return (
        <div>
            <div className="row">
                <div className="col-sm-12">
                    <h2>Dashboard</h2>
                </div>
                <div class="col-lg-12 bg-putih p-1 mb-1">
                    <div class="col-xs-12 col-sm-1">
                        <figure>
                            <img class="img-circle img-responsive" alt="" src="http://placehold.it/100x100" />
                        </figure>
                    </div>
                    <div class="col-xs-12 col-sm-11 d-flex justify-content-between">
                        <div>
                            <h5> 
                                <b>Me, </b> 
                                <span className="text-muted">add new product </span>
                                <span class="text-primary">#000</span>
                            </h5>
                            <h4><span class="text-muted">13:00 PM</span></h4>
                        </div>
                        <div>
                            <h3 className="text-warning"> View Detail</h3>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 bg-putih p-1 mb-1">
                    <div class="col-xs-12 col-sm-1">
                        <figure>
                            <img class="img-circle img-responsive" alt="" src="http://placehold.it/100x100" />
                        </figure>
                    </div>
                    <div class="col-xs-12 col-sm-11 d-flex justify-content-between">
                        <div>
                            <h5> 
                                <b>Me, </b> 
                                <span className="text-muted">add new product </span>
                                <span class="text-primary">#000</span>
                            </h5>
                            <h4><span class="text-muted">13:00 PM</span></h4>
                        </div>
                        <div>
                            <h3 className="text-warning"> View Detail</h3>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 bg-putih p-1 mb-1">
                    <div class="col-xs-12 col-sm-1">
                        <figure>
                            <img class="img-circle img-responsive" alt="" src="http://placehold.it/100x100" />
                        </figure>
                    </div>
                    <div class="col-xs-12 col-sm-11 d-flex justify-content-between">
                        <div>
                            <h5> 
                                <b>Me, </b> 
                                <span className="text-muted">add new product </span>
                                <span class="text-primary">#000</span>
                            </h5>
                            <h4><span class="text-muted">13:00 PM</span></h4>
                        </div>
                        <div>
                            <h3 className="text-warning"> View Detail</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
          );
    }
}

export default Dashboard;
