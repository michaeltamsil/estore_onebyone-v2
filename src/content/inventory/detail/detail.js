
import React, { Component } from 'react';

class Detail extends Component {
    render () {
        return (
            <div className="row">
              
                <ul className="nav nav-tabs d-flex border-bottom-0" role="tablist">
                    <li role="presentation" className="active flex-1  broder-round-0 bg-white"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Home</a></li>
                    <li role="presentation" className="flex-1  broder-round-0 bg-white"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Profile</a></li>
                    <li role="presentation" className="flex-1  broder-round-0 bg-white"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Messages</a></li>
                    <li role="presentation" className="flex-1 broder-round-0 bg-white"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">Settings</a></li>
                </ul>
               
            
                <div className="tab-content">
                <div role="tabpanel" className="tab-pane active container-fluid" id="home">
                    <div className="row">
                        <div className="col-md-6">
                            <div class="row mb-4">
                                <div class="col-md-4">
                                    <b class="text-muted">Order Number</b>
                                </div>
                                <div class="col-md-8">
                                    <span class="text-muted">#ADSd</span>
                                </div>
                            </div>
                            <div class="row mb-4">
                                <div class="col-md-4">
                                    <b class="text-muted">Order Date</b>
                                </div>
                                <div class="col-md-8">
                                    <span class="text-muted">mm dd yyyy</span>
                                </div>
                            </div>
                            <div class="row mb-4">
                                <div class="col-md-4">
                                    <b class="text-muted">Order Status</b>
                                </div>
                                <div class="col-md-8">
                                    <span class="text-muted">Picking</span>
                                </div>
                            </div>
                            <div class="row mb-4">
                                <div class="col-md-4">
                                    <b class="text-muted">Ship Date</b>
                                </div>
                                <div class="col-md-8">
                                    <span class="text-muted">mm dd yyyy</span>
                                </div>
                            </div>
                            <div class="row mb-4">
                                <div class="col-md-4">
                                    <b class="text-muted">Warehouse</b>
                                </div>
                                <div class="col-md-8">
                                    <span class="text-muted">Warehouse company 2</span>
                                </div>
                            </div>
                        </div>
                        <div className="col-md-6">
                            <div class="row mb-4">
                                <div class="col-md-4">
                                    <b class="text-muted">Order Number</b>
                                </div>
                                <div class="col-md-8">
                                    <span class="text-muted">#ADSd</span>
                                </div>
                            </div>
                            <div class="row mb-4">
                                <div class="col-md-4">
                                    <b class="text-muted">Order Date</b>
                                </div>
                                <div class="col-md-8">
                                    <span class="text-muted">mm dd yyyy</span>
                                </div>
                            </div>
                            <div class="row mb-4">
                                <div class="col-md-4">
                                    <b class="text-muted">Order Status</b>
                                </div>
                                <div class="col-md-8">
                                    <span class="text-muted">Picking</span>
                                </div>
                            </div>
                            <div class="row mb-4">
                                <div class="col-md-4">
                                    <b class="text-muted">Ship Date</b>
                                </div>
                                <div class="col-md-8">
                                    <span class="text-muted">mm dd yyyy</span>
                                </div>
                            </div>
                            <div class="row mb-4">
                                <div class="col-md-4">
                                    <b class="text-muted">Warehouse</b>
                                </div>
                                <div class="col-md-8">
                                    <span class="text-muted">Warehouse company 2</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-12">
                            <h3>Order Details</h3>
                            <table className="table border text-muted">
                                <thead>
                                    <tr>
                                        <th>NO.</th><th>PRODUCT NUMBER</th><th>SKU</th><th>QUANTITY</th><th>GOOD DETAILS</th><th>AMOUNT</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>1</td><td>1212121</td><td>qqas</td><td>qwq</td><td>wqwq</td><td>11</td>
                                    </tr>
                                    <tr>
                                        <td>1</td><td>1212121</td><td>qqas</td><td>qwq</td><td>wqwq</td><td>11</td>
                                    </tr>
                                    <tr>
                                        <td colSpan="4"></td><td>wqwq</td><td>11</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div role="tabpanel" className="tab-pane" id="profile">dfds</div>
                <div role="tabpanel" className="tab-pane" id="messages">dsfdsf</div>
                <div role="tabpanel" className="tab-pane" id="settings">asd</div>
                </div>
            </div>
          );
    }
}

export default Detail;
