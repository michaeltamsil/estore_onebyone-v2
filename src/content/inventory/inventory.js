
import React, { Component } from 'react';
import {
    BrowserRouter as Router,
    Route,
    Link,
    Switch,
  } from 'react-router-dom'
import Add from './add/add'
import './inventory.css'

const Inventory = ({ match }) => {
        return (
            <div>
            <div className="row">
              <div className="col-sm-8">
                <h2>
                  Inventory
                </h2>
              </div>
              <div className="col-sm-4 text-right mt-20">
                <div className="searchBox">
                  <div className="input-group">
                    <input type="text" className="form-control searchInput" name="SearchProduct" placeholder="search product"
                    />
                    <span className="input-group-btn">
                      <button className="btn btn-secondary" type="button">
                        <i className="fa fa-search text-muted"></i>
                      </button>
                    </span>
                  </div>
                </div>
                <button data-toggle="modal" data-target="#addForm" className="btn btn-warning btn-round pull-xs-right">
                  <i className="fa fa-plus" aria-hidden="true"></i> Add Inventory</button>
              </div>
            </div>
            <h4>Filter</h4>
            <div className="card filter" obo-filter="">
              <div className="card-block">
                {/*
                <!-- scripts/app/customers/filter -->*/}
                <form className="row child-form-group-margin-bottom-0rem">
                  <div className="col-md-2">
                    <div className="form-group">
                      <input type="text" className="form-control" name="ItemName" placeholder="Item Name" />
                    </div>
                  </div>
                  <div className="col-md-2">
                    <div className="form-group">
                      <input type="text" className="form-control" name="Code" placeholder="Code" />
                    </div>
                  </div>
                  <div className="col-md-2">
                    <div className="form-group">
                      <select className="form-control" name="ProductCategory">
                        <option defaultValue="" disabled="">Product Category</option>
                      </select>
                    </div>
                  </div>
                  <div className="col-md-2">
                    <div className="form-group">
                      <select className="form-control" name="LastOrder">
                        <option defaultValue="" disabled="">Last Order</option>
                      </select>
                    </div>
                  </div>
                  <div className="col-md-1">
                    <div className="form-group">
                      <input type="text" className="form-control" name="StockQty" placeholder="Stock Qty" title="Stock Qty" />
                    </div>
                  </div>
                  <div className="col-md-1">
                    <div className="form-group">
                      <input type="text" className="form-control" name="ReorderLevel" placeholder="Reorder Level" title="Reorder Level" />
                    </div>
                  </div>
                  <div className="col-md-2">
                    <div className="form-group center">
                      <button type="button" className="btn btn-link text-warning">
                        <i className="fa fa-times" aria-hidden="true"></i> Clear Filter</button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
            <div className="table-responsive">
              <table className="table table-condensed ">
                <thead>
                  <tr>
                    <th>ITEM NAME</th>
                    <th>CODE</th>
                    <th>PRODUCT CATEGORY</th>
                    <th>LAST ORDER</th>
                    <th>STOCK QTY</th>
                    <th>REORDER LEVEL</th>
                    <th>ACTION</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>Iphone 6 Space Grey</td>
                    <td>ACC100223334</td>
                    <td>Electronic</td>
                    <td>20/03/17</td>
                    <td>50 Unit</td>
                    <td>50</td>
                    <td>
                    <Link className="btn btn-default btn-round" to={`${match.url}/12`}>View</Link>
                    </td>
                  </tr>
                  <tr>
                    <td>Iphone 7 Gold</td>
                    <td>GLB236598661</td>
                    <td>Electronic</td>
                    <td>15/02/18</td>
                    <td>60 Unit</td>
                    <td>30</td>
                    <td>
                    <Link className="btn btn-default btn-round" to={`${match.url}/12`}>View</Link>
                    </td>
                  </tr>
                  <tr>
                    <td>Iphone 8 Space Grey</td>
                    <td>WO866952333</td>
                    <td>Electronic</td>
                    <td>03/03/18</td>
                    <td>40 Unit</td>
                    <td>40</td>
                    <td>
                      <button className="btn btn-default btn-round">View</button>
                    </td>
                  </tr>
                  <tr>
                    <td>Iphone X</td>
                    <td>HOL777555666</td>
                    <td>Electronic</td>
                    <td>04/04/18</td>
                    <td>
                      <label className="text-warning">Out</label>
                    </td>
                    <td>0</td>
                    <td>
                      <button className="btn btn-warning btn-round">View</button>
                    </td>
                  </tr>
                  <tr>
                    <td>Samsung Galaxy S9+</td>
                    <td>INT889933222</td>
                    <td>Electronic</td>
                    <td>20/03/18</td>
                    <td>10 Unit</td>
                    <td>20</td>
                    <td>
                        <Link className="btn btn-default btn-round" to='/inventory/12'>View</Link>
                    </td>
                  </tr>
                  <tr>
                    <td>Huawei P20 Pro</td>
                    <td>SOC200131369</td>
                    <td>Electronic</td>
                    <td>14/04/18</td>
                    <td>100 Unit</td>
                    <td>70</td>
                    <td>
                        <Link className="btn btn-default btn-round" to='/inventory/12'>View</Link>
                    </td>
                  </tr>
                  <tr>
                    <td>Google Pixel XL</td>
                    <td>MAD879663322</td>
                    <td>Electronic</td>
                    <td>14/03/18</td>
                    <td>5 Unit</td>
                    <td>10</td>
                    <td>
                      <button className="btn btn-default btn-round">View</button>
                    </td>
                  </tr>
                </tbody>
                
              </table>
            </div>
            <div>
              <nav className="paging pagingCustom">
                <button className="btn btn-link" type="button" tabIndex="-1" title="first" disabled="">
                  <i className="fa fa-step-backward text-muted"></i>
                </button>
                <button className="btn btn-link" type="button" tabIndex="-1" title="previous" disabled="">
                  <i className="fa fa-caret-right fa-flip-horizontal text-muted font-12em"></i>
                </button>
                <input className="form-control w-6rem" type="number" name="Page" min="1" max="1"  defaultValue="1"
                /> of
                <label name="TotalPage">2</label>
                <button className="btn btn-link" type="button" tabIndex="-1" title="next" obo-next="" disabled="disabled">
                  <i className="fa fa-caret-right font-12em" ></i>
                </button>
                <button className="btn btn-link btn-disabled" type="button" tabIndex="-1" title="last">
                  <i className="fa fa-fast-forward"></i>
                </button>
              </nav>
            </div>
            <Add />
          </div>
          );
    
}

export default Inventory;
