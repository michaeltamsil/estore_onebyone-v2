
import React, { Component } from 'react';

class Add extends Component {
    render () {
        return (
            <div className="modal fade" id="addForm" tabIndex="-1" role="dialog" aria-labelledby="addForm">
              <div className="modal-dialog modal-lg" role="document">
                <div className="modal-content">
                  <div className="modal-header border-bottom-0">
                    <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 className="modal-title" id="myModalLabel">Modal title</h4>
                  </div>
                  <div className="modal-body">
                    <div className="row">
                        <div className="col-lg-6">
                            <div className="row mb-3">
                                <div className="col-sm-4">
                                    Customer Number 
                                </div>
                                <div className="col-sm-8">
                                    <span className="text-muted">23212</span>
                                </div>
                            </div>
                            <div className="row mb-3">
                                <div className="col-sm-4">
                                    Customer Name <span className="text-danger">*</span>
                                </div>
                                <div className="col-sm-8">
                                    <input type="text" className="form-control" name="customerName" />
                                </div>
                            </div>
                            <div className="row mb-3">
                                <div className="col-sm-4">
                                    Trading As 
                                </div>
                                <div className="col-sm-8">
                                    <input type="text" className="form-control" name="tradingAs" />
                                </div>
                            </div>
                            <div className="row mb-3">
                                <div className="col-sm-4">
                                    Account Number <span className="text-danger">*</span>
                                </div>
                                <div className="col-sm-8">
                                    <input type="number" className="form-control" name="accountNumber" />
                                </div>
                            </div>
                            <div className="row mb-3">
                                <div className="col-sm-4">
                                   ABN
                                </div>
                                <div className="col-sm-8">
                                    <input type="text" className="form-control" name="ABN" />
                                </div>
                            </div>
                            <div className="row mb-3">
                                <div className="col-sm-4">
                                   ACN
                                </div>
                                <div className="col-sm-8">
                                    <input type="text" className="form-control" name="ACN" />
                                </div>
                            </div>
                            <div className="row mb-3">
                                <div className="col-sm-4">
                                    Account Owner <span className="text-danger">*</span>
                                </div>
                                <div className="col-sm-8">
                                    <input type="number" className="form-control" name="accountOwner" />
                                </div>
                            </div>
                            <div className="row mb-3">
                                <div className="col-sm-4">
                                    Date Started <span className="text-danger">*</span>
                                </div>
                                <div className="col-sm-8">
                                    <input type="date" className="form-control" name="dateStarted" />
                                </div>
                            </div>
                            <div className="row mb-3">
                                <div className="col-sm-4">
                                    Tags
                                </div>
                                <div className="col-sm-8">
                                    <div className="row">
                                        <div className="col-sm-9">
                                            <input type="text" className="form-control" />
                                        </div>
                                        <div className="col-sm-2 p-0">
                                            <button className="btn btn">Add</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-6">
                        <div className="row mb-3">
                                <div className="col-sm-4">
                                    Customer Number 
                                </div>
                                <div className="col-sm-8">
                                    <span className="text-muted">23212</span>
                                </div>
                            </div>
                            <div className="row mb-3">
                                <div className="col-sm-4">
                                    Customer Name <span className="text-danger">*</span>
                                </div>
                                <div className="col-sm-8">
                                    <input type="text" className="form-control" name="customerName" />
                                </div>
                            </div>
                            <div className="row mb-3">
                                <div className="col-sm-4">
                                    Trading As 
                                </div>
                                <div className="col-sm-8">
                                    <input type="text" className="form-control" name="tradingAs" />
                                </div>
                            </div>
                            <div className="row mb-3">
                                <div className="col-sm-4">
                                    Account Number <span className="text-danger">*</span>
                                </div>
                                <div className="col-sm-8">
                                    <input type="number" className="form-control" name="accountNumber" />
                                </div>
                            </div>
                            <div className="row mb-3">
                                <div className="col-sm-4">
                                   ABN
                                </div>
                                <div className="col-sm-8">
                                    <input type="text" className="form-control" name="ABN" />
                                </div>
                            </div>
                            <div className="row mb-3">
                                <div className="col-sm-4">
                                   ACN
                                </div>
                                <div className="col-sm-8">
                                    <input type="text" className="form-control" name="ACN" />
                                </div>
                            </div>
                            <div className="row mb-3">
                                <div className="col-sm-4">
                                    Account Owner <span className="text-danger">*</span>
                                </div>
                                <div className="col-sm-8">
                                    <input type="number" className="form-control" name="accountOwner" />
                                </div>
                            </div>
                            <div className="row mb-3">
                                <div className="col-sm-4">
                                    Date Started <span className="text-danger">*</span>
                                </div>
                                <div className="col-sm-8">
                                    <input type="date" className="form-control" name="dateStarted" />
                                </div>
                            </div>
                            <div className="row mb-3">
                                <div className="col-sm-4">
                                    Tags
                                </div>
                                <div className="col-sm-8">
                                    <div className="row">
                                        <div className="col-sm-9">
                                            <input type="text" className="form-control" />
                                        </div>
                                        <div className="col-sm-2 p-0">
                                            <button className="btn btn">Add</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                  </div>
                  <div className="border-top-0 modal-footer">
                    <button type="button" className="btn btn-default btn-round" data-dismiss="modal">Close</button>
                    <button type="button" className="btn btn-default btn-round">Save changes</button>
                  </div>
                </div>
              </div>
            </div>
          );
    }
}

export default Add;
