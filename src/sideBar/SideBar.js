import React, { Component } from 'react';
import {
    BrowserRouter as Router,
    Route,
    Link,
    Switch,
  } from 'react-router-dom'
import Dashboard from '../content/dashboard/Dashboard'
import Inventory from '../content/inventory/inventory'
import inventoryDetail from '../content/inventory/detail/detail'
import Order from '../content/order/order'
import orderDetail from '../content/order/detail/detail'

class SideBar extends Component {

    render () {
        return (
            <Router>
            <div className="row">
                    <div className="col-sm-3 col-md-2 sidebar">
                        <ul className="nav nav-sidebar">
                            <li >
                                <Link to='/'>DASHBOARD</Link>
                            </li>
                            <li >
                                <Link to='/inventory'>INVENTORY</Link>
                            </li>
                            <li >
                                <Link to='/orders'>ORDERS</Link>
                            </li>
                            <li >
                                <Link to='/setting'>SETTING</Link>
                            </li>
                        </ul>        
                    </div>
                    <div className="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
                        <Switch>
                            <Route  path='/orders/:orderId' component={orderDetail}  />
                            <Route  path='/orders' component={Order}  />
                            <Route  path='/inventory/:inventoryId' component={inventoryDetail}  />
                            <Route  path='/inventory' component={Inventory}  />
                            <Route  path='/' component={Dashboard} />
                        </Switch>
                    </div>
                </div>
            </Router>
          );
    }
}

export default SideBar;