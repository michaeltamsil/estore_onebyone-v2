import React, { Component } from 'react';
import Header from './header/Header'
import SideBar from './sideBar/SideBar'
import Login from './login/Login'

class App extends Component {
  state = {
    redirect: false
  }

  render() {
    let { redirect } = this.state
    if(redirect){
      return <Login />
    }
    return (
      <div>
        <Header />
        <div className="container-fluid">
            <SideBar />    
        </div>
      </div>
    );
  }
}

export default App;
